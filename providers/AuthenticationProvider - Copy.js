import React, { createContext, useState, useEffect } from 'react'
import Router from "next/router";
import cookies from 'js-cookie'
import * as cookie from 'cookie'
const hasWindow=()=>
{
  return typeof window !== 'undefined';
  

}
export const UserContext = createContext()

const AuthenticationProvider = ({ children }) => {
    const [token, setToken] = useState(null);
    const [locale, setLocale] = useState(null);
    const renderer=hasWindow()?'browser':'server';

    useEffect(() => {
        if (cookies.get('aap-auth-token')) {
            console.log(cookies.get('aap-auth-token'))
            setToken(cookies.get('aap-auth-token'));
        }
        else{
            Router.push("/login")
        }
        if (localStorage.getItem("i18nextLng") != undefined){
            setLocale(localStorage.getItem("i18nextLng"))
        }
        else{
            console.log("no locale")
        }
    }, [token, locale]);
    return (
        <UserContext.Provider
            value={{
                token: token,
                setToken: setToken,
                locale:locale,
                setLocale:setLocale
            }}
        >
            {children}
        </UserContext.Provider>
    )
}

export default AuthenticationProvider;

export function requireAuthentication(gssp) {
    return async (context) => {
       
        if (context.req.headers.cookie) {
           
            const parsedCookies = cookie.parse(context.req.headers.cookie);
         
            const tokenUuid = Object.entries(parsedCookies)[0][0];
         
            if(tokenUuid=='aap-auth-token')
            {
                const tokenEnc=Object.entries(parsedCookies)[0][1];
           
            }
            else{
                return{
                    redirect: { destination: '/login', permanent: false }
                  }}

            

          }
          else {
            return{
            redirect: { destination: '/login', permanent: false }
          }}
        return await gssp(context); // Continue on to call `getServerSideProps` logic
    }
}

