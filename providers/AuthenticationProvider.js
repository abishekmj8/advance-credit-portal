import React, { useState, useEffect, createContext } from 'react'
import App from 'next/app'
import Router from "next/router";
import cookies from 'js-cookie'
import * as cookie from 'cookie'
const hasWindow = () => {
    return typeof window !== 'undefined';


}
export const UserContext = createContext()

class AuthenticationProvider extends App {
    state = {
        token: null,
        locale: null
    }

    componentDidMount = () => {
        const token = cookies.get('aap-auth-token');
        let locale = localStorage.getItem("i18nextLng");
        if (token) {
            let tokenDetail = this.parseJwt(token)
            this.setState({ token })
        }
        else {
            Router.push("/login")
        }
        this.setState({ locale });
    }
    setToken = (token) => {
        this.setState({ token: token })
    }
    setLocale = (locale) => {
        this.setState({ locale: locale });
    }
    parseJwt(token) {
        if (!token) { return; }
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        console.log(JSON.parse(window.atob(base64)))
        return JSON.parse(window.atob(base64));
    }
    
    render() {
        return (
            <UserContext.Provider
                value={{
                    token: this.state.token,
                    setToken: this.setToken,
                    locale: this.state.locale,
                    setLocale: this.setLocale
                }}
            >
                {this.props.children}
            </UserContext.Provider>
        )
    }
}

export default AuthenticationProvider;

export function requireAuthentication(gssp) {
    return async (context) => {

        if (context.req.headers.cookie) {

            const parsedCookies = cookie.parse(context.req.headers.cookie);

            const tokenUuid = Object.entries(parsedCookies)[0][0];

            if (tokenUuid == 'aap-auth-token') {
                const tokenEnc = Object.entries(parsedCookies)[0][1];

            }
            else {
                return {
                    redirect: { destination: '/login', permanent: false }
                }
            }



        }
        else {
            return {
                redirect: { destination: '/login', permanent: false }
            }
        }
        return await gssp(context); // Continue on to call `getServerSideProps` logic
    }
}

