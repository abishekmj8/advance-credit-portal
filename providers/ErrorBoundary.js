import React, { createContext, useState, useEffect } from 'react'
export const ErrorContext = createContext()
const ErrorBoundary = ({ children }) => {
  const errorReporting = (error, info) => {
    if (!info) {
      info = "No additional information Provided!";
      console.log(error, info)
    }
    else {
      console.log(error, info)
    }
  }
  return (
    <ErrorContext.Provider
        value={{
          errorReporting:errorReporting
        }}
    >
        {children}
    </ErrorContext.Provider>
)
}
export default ErrorBoundary