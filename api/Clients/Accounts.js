// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

// export default function handler(req, res) {
//   res.status(200).json({ name: 'John Doe' })
// }


import { ApiClient } from '@/api/Middleware/Dashboard'
let client = new ApiClient(process.env.NEXT_PUBLIC_CUSTOMER_SERVICE)

export default {
  accountSummary(data) {

    return client.getclientWithHeader.get(`customers/${data}/account-summary`)
  },
  customersInvoices(data) {

    return client.getclientWithHeader.get(`customers/invoices?customerAccountNumber=${data}`)
  },
}