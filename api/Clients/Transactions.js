import { ApiClient } from '@/api/Middleware/Dashboard'
let client = new ApiClient(process.env.NEXT_PUBLIC_TRANSACTION_SERVICE)

export default {
  recentTransactions(data) {

    return client.getclientWithHeader.get(`transactions/${data}/recent-transactions`);
  },
  invoiceDetails(data) {
    return client.getclientWithHeader.get(`transactions/${data}`);
  },
  paymentHeaderDetails(data) {
    return client.getclientWithHeader.get(`transactions/${data}/paymentHeader`);
  },
  paymentDetails(data) {
    return client.getclientWithHeader.get(`transactions/${data}/paymentDetails`);
  },
  invoiceTransactionActivity(data) {
    return client.getclientWithHeader.get(`transactions/${data}/activity`);
  },
}