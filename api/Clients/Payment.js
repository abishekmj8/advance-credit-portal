// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { ApiClient } from '@/api/Middleware/Dashboard'



let client = new ApiClient(process.env.NEXT_PUBLIC_API_PAYMENT_SERVICE)

export default {
  getSavedCardDetails(data) {


   
       return client.getclientWithHeader.get('cybersource-gateway/customerCards', { params:data })
  },
  makePayment(data) {
    return client.postclientWithHeader.post('cybersource-gateway/makePayment', data)
  },
  getPublicKey(data) {
    return client.getclientWithHeader.get('cybersource-gateway/getPublicKey', { params:data })
  },

}