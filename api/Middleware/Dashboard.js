import axios from 'axios';
import Base64 from 'crypto-js/enc-base64';
import { HmacSHA256 } from 'crypto-js';
import cookies from 'js-cookie'


const getClientWithHeaderNoAuth = (baseUrl = null) => {

  const applicationId="aapfinancebill";
  const host="appfinance.app.com"
  const secretkey='secretkey';
  const date=Date.now(); //UTC Coordinated Universal Time

  const headerData="hostvalue datevalue applicationId";
  const headervalue="hostvalue:"+host+" datevalue:"+date+" applicationId:"+applicationId;

  const hmacstring= HmacSHA256(headervalue,secretkey);
  const hmacDigest = Base64.stringify(hmacstring);

  
  const signature="headers="+headerData+","+"signature="+hmacDigest;


  const options = {
    baseURL: baseUrl,
    headers: {
        applicationId:applicationId,
        signature:signature,
        datevalue:date,
        hostvalue:host,
      
    }
  };

  const client = axios.create(options);

  client.interceptors.request.use(
    async function (config) {
      return config;
    },
    function (error) {
      console.log(error)
        
    }
  );

  // Add a response interceptor
  client.interceptors.response.use((response) => {
    return response;
    }, (error) => {
    });
    
    
    client.interceptors.request.use(
    async function (config) {
    return config;
    },
    function (error) {
      
    }
    );

  return client;
};

const getClientWithHeader = (baseUrl = null) => {

  const applicationId="aapfinancebill";
  const host="appfinance.app.com"
  const secretkey='secretkey';
  const date=Date.now(); //UTC Coordinated Universal Time

  const headerData="hostvalue datevalue applicationId";
  const headervalue="hostvalue:"+host+" datevalue:"+date+" applicationId:"+applicationId;

  const hmacstring= HmacSHA256(headervalue,secretkey);
  const hmacDigest = Base64.stringify(hmacstring);

  
  const signature="headers="+headerData+","+"signature="+hmacDigest;


  const options = {
    baseURL: baseUrl,
    headers: {
        applicationId:applicationId,
        signature:signature,
        datevalue:date,
        hostvalue:host,
        Authorization:  `Bearer ${cookies.get('aap-auth-token')}`,
      
    }
  };

  const client = axios.create(options);

  client.interceptors.request.use(
    async function (config) {
      return config;
    },
    function (error) {
        
    }
  );

  // Add a response interceptor
  client.interceptors.response.use((response) => {
    return response;
    }, (error) => {
    });
    
    
    client.interceptors.request.use(
    async function (config) {
    return config;
    },
    function (error) {
      console.log(error)
    }
    );

  return client;
};

const postClientWithHeader = (baseUrl = null) => {

  const applicationId="aapfinancebill";
  const host="appfinance.app.com"
  const secretkey='secretkey';
  const date=Date.now(); //UTC Coordinated Universal Time

  const headerData="hostvalue datevalue applicationId";
  const headervalue="hostvalue:"+host+" datevalue:"+date+" applicationId:"+applicationId;

  const hmacstring= HmacSHA256(headervalue,secretkey);
  const hmacDigest = Base64.stringify(hmacstring);

  
  const signature="headers="+headerData+","+"signature="+hmacDigest;


  const options = {
    baseURL: baseUrl,
    headers: {
        applicationId:applicationId,
        signature:signature,
        datevalue:date,
        hostvalue:host,
        Authorization:  `Bearer ${cookies.get('aap-auth-token')}`,
      
    }
  };

  const client = axios.create(options)

  return client;
};

class ApiClient {
  constructor(baseUrl = null) {
  
    this.getclientWithHeaderNoAuth = getClientWithHeaderNoAuth(baseUrl);
    this.getclientWithHeader = getClientWithHeader(baseUrl);
    this.postclientWithHeader = postClientWithHeader(baseUrl);
  }

  post(url, data = {}, conf = {}) {

    return this.clientWithHeader

      .post(url, data, conf)

      .then((response) => Promise.resolve(response))

      .catch((error) => Promise.reject(error));

  }

  get(url, conf = {}) {

    try {

      const response = this.getClientWithHeader

        .get(url, conf);

      return Promise.resolve(response);

    } catch (error) {

      console.log(error);

    }

  }
}

export { ApiClient };