import React, { useEffect, useState, useContext ,useRef } from "react";

import DefaultLayout from "@/layout/default";
import Header from "@/components/common/Header";
import AuthNavbar from "@/components/common/Navbar"
import { Grid, _ } from "gridjs-react";
import "gridjs/dist/theme/mermaid.css";
import Accounts from "@/api/Clients/Accounts";
import { ErrorContext } from "@/providers/ErrorBoundary";
import { requireAuthentication } from "@/providers/AuthenticationProvider";
import {
    Row,
    Col,
    Container,
    Card,
    CardHeader,
    CardBody,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    FormGroup,
    Form,
    InputGroup,
    Button, Progress, Label, Input
} from "reactstrap";
import swal from 'sweetalert';
import PaymentModal from "@/components/modal/paymentModal";
import {Translate} from "@/components/Translations/translate";
const InvoicePay = (props) => {
    const { errorReporting } = useContext(
        ErrorContext
    );
    useEffect(() => {
        customersInvoices.current()
    }, []);
    const [modalOpen, setModalOpen] = useState(false);
    let [data, setRecentTransaction] = useState([]);
    //generated payment amount
    let [invoiceRequestArray, setselectedInvoice] = useState([]);
    let [Total, setsubTotal] = useState(0);


    let selectedInvoice = 0;
    let selectedCredit = 0;
    let invoiceTotal = 0;
    let appliedCredits = 0;
    let subTotal = 0;
    let selectedArray = [];
    let invoiceRequest = [];
    //end



    const customersInvoices =useRef(async () => {
        try {
            let accountNumber = "AAP001";
            if (accountNumber) {
                const response = await Accounts.customersInvoices(accountNumber)

                if (response.status == 200) {
                    setRecentTransaction(response.data.invoices)
                }
                else {
                    setRecentTransaction(null)

                }

            }


        }
        catch (e) {
            console.log(e)
            errorReporting(e,"customersInvoices Function Fail")
        }

    });


    const updateCart = (invoice) => {
        if (invoice) {
            let obj = {
                invoiceId: invoice.invoiceId,
                amount: invoice.amount,
                type: invoice.type
            }
            let invoiceObj = {
                invoiceId: invoice.invoiceId,
                invoiceNumber: invoice.invoiceNumber,
                customerAccountNumber: invoice.customerAccountNumber,
                type: invoice.type,
                paymentTerms: invoice.paymentTerms,
                amount: invoice.amount,
                date: invoice.date,
                dueDate: invoice.dueDate,
                transactionBalance: invoice.transactionBalance
            }
            selectedInvoice = 0;
            selectedCredit = 0;
            invoiceTotal = 0;
            appliedCredits = 0;
            subTotal = 0;
            if (document.getElementById(invoice.invoiceId).checked == true) {
                selectedArray.push(obj)
                invoiceRequest.push(invoiceObj)
                document.getElementById(invoice.invoiceNumber).value = invoice.amount;
                document.getElementById(invoice.invoiceNumber).disabled = false
            }
            else {
                // selectedArray.pop(obj)
                let index = selectedArray.indexOf(obj)
                selectedArray.splice(index, 1);
                let i = invoiceRequest.indexOf(invoiceObj)
                invoiceRequest.splice(i, 1);
                document.getElementById(invoice.invoiceNumber).value = "";
                document.getElementById(invoice.invoiceNumber).disabled = true;
            }
            selectedArray.forEach((el) => {
                if (el.type == "APP-Inv-Conversion") {
                    invoiceTotal += Number(el.amount);
                    selectedInvoice += 1;
                }
                if (el.type == "Credit-Memo") {
                    appliedCredits += Number(el.amount);
                    selectedCredit += 1
                }
            })
            // console.log(selectedArray)
            // console.log(invoiceRequest)
            subTotal = invoiceTotal + appliedCredits;
            if(selectedArray.length && subTotal > 0){
                document.getElementById("payBtn").disabled=false;
            }
            else {
                document.getElementById("payBtn").disabled=true;
            }
            document.getElementById("selectedInvoice").innerText = selectedInvoice
            document.getElementById("invoiceTotal").innerText = invoiceTotal
            document.getElementById("selectedCredit").innerText = selectedCredit
            document.getElementById("appliedCredits").innerText = appliedCredits
            document.getElementById("subTotal").innerText = subTotal
        }
        else{
            document.getElementById("payBtn").disabled=true;
        }
    }

    const updateCustumAmount = (invoice, customAmount) => {
        if ((invoice.type == "APP-Inv-Conversion" && customAmount <= invoice.amount) || (invoice.type == "Credit-Memo" && customAmount >= invoice.amount && customAmount <= 0)) {
            let obj = {
                invoiceId: invoice.invoiceId,
                amount: customAmount,
                type: invoice.type
            }
            let invoiceObj = {
                invoiceId: invoice.invoiceId,
                invoiceNumber: invoice.invoiceNumber,
                customerAccountNumber: invoice.customerAccountNumber,
                type: invoice.type,
                paymentTerms: invoice.paymentTerms,
                amount: invoice.amount,
                date: invoice.date,
                dueDate: invoice.dueDate,
                transactionBalance: customAmount
            }
            selectedArray.forEach((el) => {
                if (el.invoiceId == invoice.invoiceId) {
                    let index = selectedArray.indexOf(obj)
                    selectedArray.splice(index, 1);
                    selectedArray.push(obj)
                }
            });
            invoiceRequest.forEach((el) => {
                if (el.invoiceId == invoice.invoiceId) {
                    let i = invoiceRequest.indexOf(obj)
                    invoiceRequest.splice(i, 1);
                    invoiceRequest.push(invoiceObj)
                }
            })
            selectedInvoice = 0;
            selectedCredit = 0;
            invoiceTotal = 0;
            appliedCredits = 0;
            subTotal = 0;
            console.log("selectedArray", selectedArray)
            selectedArray.forEach((el) => {
                if (el.type == "APP-Inv-Conversion") {
                    invoiceTotal += Number(el.amount);
                    selectedInvoice += 1;
                }
                if (el.type == "Credit-Memo") {
                    appliedCredits += Number(el.amount);
                    selectedCredit += 1
                }
            })
            subTotal = invoiceTotal + appliedCredits;
            document.getElementById("selectedInvoice").innerText = selectedInvoice
            document.getElementById("invoiceTotal").innerText = invoiceTotal
            document.getElementById("selectedCredit").innerText = selectedCredit
            document.getElementById("appliedCredits").innerText = appliedCredits
            document.getElementById("subTotal").innerText = subTotal
        }
        else {
            selectedArray.forEach((el) => {
                if (el.invoiceId == invoice.invoiceId) {
                    document.getElementById(invoice.invoiceNumber).value = el.amount;
                }
            })
            swal({
                //title: "Someting went Wrong!",
                text: `Please enter an amount less than ${invoice.amount} USD`,
                icon: "error",
            })
        }
    }
    const checkout = () => {
        if (invoiceRequest.length != 0 && selectedArray.length != 0) {
            setselectedInvoice(invoiceRequest)
            setsubTotal(subTotal)
            setModalOpen(true)
        }
    }
    const modalClose = () => {
        console.log("modalClose")
        selectedInvoice = 0;
        selectedCredit = 0;
        invoiceTotal = 0;
        appliedCredits = 0;
        subTotal = 0;
        subTotal = invoiceTotal + appliedCredits;
        document.getElementById("selectedInvoice").innerText = selectedInvoice
        document.getElementById("invoiceTotal").innerText = invoiceTotal
        document.getElementById("selectedCredit").innerText = selectedCredit
        document.getElementById("appliedCredits").innerText = appliedCredits
        document.getElementById("subTotal").innerText = subTotal
        setModalOpen(false)
    }
    const switchModal = (data) => {
        console.log(data)
    }
    return (
        <>
            <Header />
            {/* Page content */}
            <AuthNavbar />
            <Container className="mt--12" fluid>
                <Row className="my-5">
                    <Col xl="9">
                        <Grid

                            data={data}
                            columns={[
                                {
                                    data: (data) => _(<div><input name={data.type} type="checkbox" id={data.invoiceId} onClick={event => updateCart(data)} /></div>),
                                    name: "Select",
                                    attributes: {
                                        'title':  'Select'
                                      }
                                },
                                {
                                    data: (data) => data.date,
                                    name: "Date",
                                    attributes: {
                                        'title':  'Date'
                                      }
                                },
                                {
                                    data: (data) => data.type,
                                    name: "Type",
                                    attributes: {
                                        'title':  'Type'
                                      }
                                },
                                {
                                    data: (data) => _(<a href="#" onClick={() => switchModal(data)}>{data.invoiceNumber}</a>),
                                    name: "Invoice #",
                                    attributes: {
                                        'title':  'Invoice #'
                                      }
                                },
                                {
                                    data: (data) => "",
                                    name: "PO #",
                                    attributes: {
                                        'title':  'PO #'
                                      }
                                },
                                {
                                    data: (data) => data.paymentTerms,
                                    name: "Payment Terms",
                                    attributes: {
                                        'title':  'Payment Terms'
                                      }
                                },
                                {
                                    data: (data) => data.dueDate,
                                    name: "Due Date",
                                    attributes: {
                                        'title':  'Due Date'
                                      }
                                },
                                {
                                    data: (data) => data.amount,
                                    name: "Amount",
                                    attributes: {
                                        'title':  'Amount'
                                      }
                                },
                                {
                                    data: (data) => data.transactionBalance,
                                    name: "Transaction Balance",
                                    attributes: {
                                        'title':  'Transaction Balance'
                                      }
                                },
                                {
                                    data: (data) => _(<Input type="number" id={data.invoiceNumber} disabled onChange={(e) => updateCustumAmount(data, e.target.value)} />),
                                    name: "Balance Amount",
                                    attributes: {
                                        'title':  'Balance Amount'
                                      }
                                },
                            ]}
                            search={true}
                            sort={true}
                            resizable={false}
                            pagination={{
                                enabled: true,
                                limit: 7,
                            }}
                        />
                    </Col>
                    <Col xl="3" className="mt-5">

                        <Card className="shadow">
                            <CardHeader className="border-0">
                                <Row className="align-items-center">
                                    <div className="col-12">
                                        <h3 className="mb-0">{Translate("Pay_Selected_Invoices")}</h3>
                                    </div>
                                    <div className="col text-right">

                                    </div>
                                </Row>
                            </CardHeader>
                            <CardBody>
                                <Row className="align-items-center">
                                    <Col xl="6" className="text-left">{Translate("Selected_Invoices")}</Col>
                                    <Col xl="6" className="text-right" id="selectedInvoice">{0}</Col>
                                </Row>
                                <Row className="align-items-center">
                                    <Col xl="6" className="text-left">{Translate("Selected_Credit")}</Col>
                                    <Col xl="6" className="text-right" id="selectedCredit" >{0}</Col>
                                </Row>
                                <Row className="align-items-center">
                                    <Col xl="6" className="text-left">{Translate("Invoice_Total")}</Col>
                                    <Col xl="6" className="text-right">$<span id="invoiceTotal">{0}</span></Col>
                                </Row>
                                <Row className="align-items-center">
                                    <Col xl="6" className="text-left">{Translate("Applied_Credits")}</Col>
                                    <Col xl="6" className="text-right">$<span id="appliedCredits">{0}</span></Col>
                                </Row>
                                <Row className="align-items-center">
                                    <Col xl="6" className="text-left h3">{Translate("Subtotal")}</Col>
                                    <Col xl="6" className="text-right">$<span id="subTotal">{0}</span></Col>
                                </Row>
                                <Row className="align-items-center">
                                    <Col xl="12" >

                                        <Button onClick={() => checkout()} id="payBtn" className="make-payment my-4 w-100" color="primary" type="button" >
                                        {Translate("Checkout")}
                                        </Button>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
            {modalOpen &&
                <PaymentModal toggle={() => setModalOpen(!modalOpen)} modalOpen={modalOpen} amount={Total} invoicesRequest={invoiceRequestArray} modalClose={modalClose}/>
            }
        </>
    )
}
InvoicePay.getLayout = function getLayout(page) {
    return (
        <DefaultLayout>
            {page}
        </DefaultLayout>
    )
}
export default InvoicePay;
export const getServerSideProps = requireAuthentication(context => {

    return { props: {} };
  })