import React, { useEffect, useState, useContext } from "react";
import DefaultLayout from "@/layout/default";
import Header from "@/components/common/Header";
import AuthNavbar from "@/components/common/Navbar"
import BreadCrumb from "@/components/common/BreadCrumb"
import {
    Row,
    Col,
    Container,
    Card,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    FormGroup,
    Form,
    InputGroup,
    Button
  } from "reactstrap";
  import DatePicker from "react-datepicker";
  import "react-datepicker/dist/react-datepicker.css";
  const Payments = (props) => {
    let [selectedAmount, setselectedAmount] = React.useState("PAYMENT AMOUNT");
    let [selectedCard, setselectedCard] = React.useState("PAYMENT SOURCE");
    const [cardData, setCardData] = React.useState(null);
    const [amountData, setAmountData] = React.useState(null);
    let [paymentDate, setpaymentDate] = React.useState('24-DEC-2021');
    return (
        <>
            <Header />
            {/* Page content */}
            <AuthNavbar />
            <Container className="mt--12" fluid>
            <BreadCrumb />
                {/* <Row className="mb-1">
                    <Col xl="12">
                        <PaymentNavbar 
                            tabs={[
                                {
                                    "name": "MAKE A PAYMENT",
                                    "value": "make_payment"
                                },
                                {
                                    "name": "PENDING PAYMENTS",
                                    "value": "pending_payment"
                                },
                                {
                                    "name": "AUTOMATIC PAYMENTS",
                                    "value": "automatic_payment"
                                },
                                {
                                    "name": "PAYMENT HISTORY",
                                    "value": "payment_history"
                                },
                                {
                                    "name": "SAVED PAYMENT CARTS",
                                    "value": "saved_payment_carts"
                                }
                            ]}
                        />
                    </Col>
                </Row> */}
                <Row className="mb-5">
                    <Col xl="3">
                        <Card className="shadow" style={{ height: "175px" }}>
                            <h5 className="ml-4 mt-3 text-uppercase">Balance Due</h5>
                            <h1 className="ml-4">5,980.45</h1>
                            <h5 className="ml-4">Payment Date: 25-NOV-20</h5>
                        </Card>
                    </Col>
                    <Col xl="3">
                        <Card className="shadow" style={{ height: "175px" }}>
                            <h5 className="ml-4 mt-3 text-uppercase">Current Balance</h5>
                            <h1 className="ml-4">9,762.90</h1>
                            <h5 className="ml-4">Statement Balance: 5980.45</h5>
                            <h5 className="ml-4" style={{ textDecoration: "underline" }}>25-NOV-20 Statement</h5>
                        </Card>
                    </Col>
                    <Col xl="3">
                        <Card className="shadow" style={{ height: "175px" }}>
                            <h5 className="ml-4 mt-3 text-uppercase">available credit</h5>
                            <h1 className="ml-4">6,217.55</h1>
                            <h5 className="ml-4">Credit limit: 10000</h5>
                        </Card>
                    </Col>
                    <Col xl="3"><Card className="shadow" style={{ height: "175px" }}>
                        <h5 className="ml-4 mt-3 text-uppercase">Pay selected invoices</h5>
                        <h5 className="ml-4">Open Invoices: <span style={{fontWeight:"bold"}}>19,762.90</span></h5>
                        <div className="text-center">
                            <Button className="my-4 btn-x-large" style={{border:"solid 2px #cc0033",color: "#cc0033"}}type="button">
                               Pay Select Invoices
                            </Button>
                        </div>
                    </Card>
                    </Col>
                </Row>

                <Row className="mb-5">
                    <Col xl="6">
                        <h5 className="ml-4 text-uppercase">No Scheduled Auto Payment</h5>
                    </Col>
                    <Col xl="6">
                        <h5 className="ml-4 text-right" style={{ textDecoration: "underline" }}>Setup Auto Payment</h5>
                    </Col>
                </Row>
                <Form role="form">
                    <h5 className="ml-4 text-uppercase">Make a Payment</h5>
                    <Row className="mb-5">
                        <Col xl="3">
                            <FormGroup>
                                <InputGroup className="mb-3 w-100">
                                    <UncontrolledDropdown className="w-100">
                                        <DropdownToggle
                                            caret
                                            color="secondary"
                                            id="dropdownMenuButton"
                                            type="button"
                                            className="w-100"
                                        >
                                            {selectedAmount}
                                        </DropdownToggle>
                                        <DropdownMenu aria-labelledby="dropdownMenuButton">
                                            {amountData && amountData.map((data, i) =>
                                                <DropdownItem  id={data.id} name="amount" onClick={(e) => toggleOption(e)} key={i}>
                                                    {data.amount_type}
                                                </DropdownItem>
                                            )}
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </InputGroup>
                            </FormGroup>
                        </Col>
                        <Col xl="3">
                            <FormGroup>
                                <InputGroup className="mb-3 w-100">

                                    {/* <Input value="GOODYEAR TIRE CENTER" type="text" /> */}
                                    <UncontrolledDropdown className="w-100">
                                        <DropdownToggle
                                            caret
                                            color="secondary"
                                            id="dropdownMenuButton"
                                            type="button"
                                            className="w-100"
                                        >
                                            {selectedCard}
                                        </DropdownToggle>
                                        <DropdownMenu aria-labelledby="dropdownMenuButton">
                                            {cardData && cardData.map((data, i) =>
                                                <DropdownItem  id={data.id} name="card" onClick={(e) => toggleOption(e)} key={i}>
                                                    {data.card_name}
                                                </DropdownItem>
                                            )}
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </InputGroup>
                            </FormGroup>
                        </Col>
                        <Col xl="3">
                           
                                  
                                <FormGroup>
                      <InputGroup className="mb-3">
                       
                        {/* <Input placeholder="Payment Date" defaultValue="28-DEC-2021" type="date" /> */}
                        <DatePicker 
                        name="payment_date"
                        className="btn w-100 btn-secondary"
                        id= "example-datepicker" 
                    value   = {paymentDate} 
                    dateFormat ="MM/DD/YYYY"
                    onChange={(e) => toggleOptionDate(e)} />

                      </InputGroup>
                    </FormGroup>
                               
                        </Col>
                        <Col xl="3">
                            <div className="text-center">
                                <Button className="" color="primary" type="button">
                                    Make a Payment
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </Container>
        </>
    )
  }
  Payments.getLayout = function getLayout(page) {
    return (
      <DefaultLayout>
        {page}
      </DefaultLayout>
    )
  }
  export default Payments;