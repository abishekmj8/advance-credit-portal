import React, { useContext } from "react";
import Router from "next/router";
import {
    Button,
    Card,
    CardBody,
    FormGroup,
    Form,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Row,
    Col,
    Container,
  } from "reactstrap";
  import Image from 'next/image';
  import Validate from "@/api/Clients/Validate";
  import { UserContext } from "@/providers/AuthenticationProvider";
  import { ErrorContext } from "@/providers/ErrorBoundary"
  import cookie from "js-cookie";
  import Logo from "@/components/common/logo";
  const Login =() => {
    const { errorReporting } = useContext(
      ErrorContext
  );
    let [userName, setuserName] = React.useState('');
    let [credentialError, setcredentialError] = React.useState(false);
    const { token, setToken, locale, setLocale } = useContext(
      UserContext
    );

    const handleChange=(e)=>{
     
        setuserName(e.target.value)
  
  }


  function ValidateEmail(mail) 
{
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
  {
    return (true)
  }
   
    return (false)
}


const validateCredentials  = async   () => {
    try {
        const formData = { emailAddress:userName };
        const response = await Validate.validateCredentials(formData);
        if (response.status===200)
        {
            cookie.set("aap-auth-token", response.data);
            //console.log(response.data)
            setToken(response.data)
            Router.push(`/dashboard/${locale}`);
            setTimeout(() => {
            Router.reload()
            },1000)
        }
        else{

          //************ */
          // cookie.set("aap-auth-token","eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0QGdtYWlsLmNvbSxBQVBDVVNUMDAxIiwiaXNzIjoiYWFwQXV0aFNlcnZpY2UiLCJpYXQiOjE2NDY5ODk0MzEsImV4cCI6MTY0NzA3NTgzMX0.ONhCvICLd7P7QHp8Q51_MKUsfXN7kikVPEaxqy1fW_chnA9kreq1561hlYr1WUUidEw5LxC8LbZMEcLifRW24A")
          // Router.push("/dashboard/en");

          /************** */
            
          setcredentialError(true)
        }
       

        return true;
    }
    catch(e)
    {
        errorReporting(e,"validateCredentials Function Fail")
        return false;

    }
}

  const LoginValidate=async ()=>{
   const valid= ValidateEmail(userName)
   if(!valid)
   {
       setcredentialError(true)
       console.log('Invalid Credentials')
   }
   else{
    setcredentialError(false)
    console.log('Valid Credentials')
    //validation API
    const valid= await validateCredentials();
    console.log("valid crential",valid)

   }
     
}




    return (
      <>
      <Container>
      <Row className="mt-6">
        <Col lg="5" md="7" className="mx-auto">
            
          <Card className="bg-secondary shadow border-0">
         
            <CardBody className="px-lg-5 py-lg-5">
              <div className="text-center mb-5">
                  <Logo/>
              {/* <img src={Logo} alt="Logo" height='50' width='200' /> */}
               
              </div>
            

                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                     
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Email"
                      type="email"
                      autoComplete="new-email"
                      onChange={(e) => handleChange(e)} 
                    />
                  </InputGroup>
                </FormGroup>
              
                <div className="custom-control custom-control-alternative custom-checkbox">
                  <input
                    className="custom-control-input"
                    id=" customCheckLogin"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor=" customCheckLogin"
                  >
                    <span className="text-muted">Remember me</span>
                  </label>
                </div>
                <div className="text-center">
                  <Button className="my-4" color="primary" type="button"
                  onClick={(e) => LoginValidate(e)} 
                  >
                    Sign in
                  </Button>
                </div>
              {credentialError&&
              <div className="text-center">
                <small className="text-danger">Invalid Credentails</small>
              </div>
              }
            </CardBody>
          </Card>
        
        </Col>
        </Row>
        </Container>
      </>
    );
  };
  
  export default Login;
  