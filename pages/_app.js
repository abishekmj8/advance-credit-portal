import "@/styles/css/aap-necleo.css";
import "@fortawesome/fontawesome-free";
import "@/styles/scss/aap-billing-portal-dashboard.scss";
import Router from "next/router";
import ReactDOM from "react-dom";
import PreLoader from "@/components/common/PreLoader";
import i18n from "@/components/Translations/i18n"
import { useTranslation } from "react-i18next";
import AuthenticationProvider from "@/providers/AuthenticationProvider"
import ErrorBoundary from "@/providers/ErrorBoundary"
import Head from 'next/head';
Router.events.on("routeChangeStart", (url) => {

 


  document.body.classList.add("body-page-transition");
  // ReactDOM.render(
  //   <PreLoader path={url} />,
  //   document.getElementById("page-transition")
  // );
});
Router.events.on("routeChangeComplete", (url) => {



  ReactDOM.unmountComponentAtNode(document.getElementById("page-transition"));
  document.body.classList.remove("body-page-transition");
});
Router.events.on("routeChangeError", (url) => {



  ReactDOM.unmountComponentAtNode(document.getElementById("page-transition"));
  document.body.classList.remove("body-page-transition");
});



const AapMainApp = ({ Component, pageProps }) => {


  const getLayout = Component.getLayout || ((page) => page)
  return getLayout(
    
    <ErrorBoundary>
            <Head>
         <title>Advance Pro Credit Portal</title>
         </Head>
        
      <AuthenticationProvider>
 
        <Component {...pageProps} />
      </AuthenticationProvider>
    </ErrorBoundary>
  );
}

export default AapMainApp
