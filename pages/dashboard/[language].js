import React, { useEffect, useState, useContext } from "react";
import DefaultLayout from "@/layout/default";
import Header from "@/components/common/Header";
import AuthNavbar from "@/components/common/Navbar"
import BreadCrumb from "@/components/common/BreadCrumb"
import AccountSummary from "@/components/common/AccountSummary";
import RecentTransaction from "@/components/common/RecentTransaction";
import OpenToBuy from "@/components/common/OpenToBuy";

import {
  Row,
  Col,
  Container
} from "reactstrap";
import { requireAuthentication } from "@/providers/AuthenticationProvider";
const Dashboard = (props) => {
  let [availablePercentage, setAvailablePercentage] = React.useState(0);
  let [totalCreditLimit, setTotalCreditLimit] = React.useState(0);


  return (
    <>
      <Header />
      <AuthNavbar />
      <Container className="mt--12" fluid>
        <BreadCrumb />
        <Row className="mt-5">
          <Col xl="5">
            <Row>
              <Col xl="12" className="mb-3">
                <AccountSummary 
                setTotalCreditLimit={setTotalCreditLimit} 
                setAvailablePercentage={setAvailablePercentage} />
              </Col>
              <Col xl="12" className="mt-3">
                <OpenToBuy 
                totalCreditLimit={totalCreditLimit} 
                availablePercentage={availablePercentage} />
              </Col>
            </Row>
          </Col>
          <Col className="mb-5 mb-xl-0" xl="7">
            <RecentTransaction />
          </Col>
        </Row>
      </Container>
    </>
  );
};
//Get page specific Layout.
Dashboard.getLayout = function getLayout(page) {
  return (
    <DefaultLayout>
      {page}
    </DefaultLayout>
  )
}

export default Dashboard;
export const getServerSideProps = requireAuthentication(context => {

  return { props: {} };
})