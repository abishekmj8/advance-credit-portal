import React from "react";
import MainHeader from '@/components/common/MainHeader';
import MainFooter from '@/components/common/MainFooter';
import {
  Container
} from "reactstrap";
const defaultLayout = (props) => {

  let mainContentRef = React.createRef();

  return (
    <>
      <div className="main-content" ref={mainContentRef}>
        <MainHeader {...props}
          logo={{
            innerLink: "/admin/index",
            imgSrc: "/img/brand/advance.png",
            imgAlt: "...",
          }}
          brandText={'Advance Pro Credit Portal'}
        />
        {props.children}
        <Container fluid>
          <MainFooter />
        </Container>
      </div>
    </>
  );
}
export default defaultLayout;