// Import Highcharts
import Highcharts from "highcharts";
import HighchartsReact from 'highcharts-react-official'
import drilldown from "highcharts/modules/drilldown.js";
import React, { useState } from "react";
if (typeof Highcharts === 'object') {
    drilldown(Highcharts);
}


const pie = ({ availablePercentage,totalCreditLimit }) => {


    var chartOptions = {
        chart: {
            type: 'pie',
        },
           legend: {
        align: 'center',
        verticalAlign: 'top',
        layout: 'horizontal',
        x: 0,
        y: 0
    },
        title: {
            text: `${availablePercentage} %`,
            align: 'center',
            verticalAlign: 'middle',
            y: 35,// change this to 0
            style: {
                fontSize: '15px'
            }
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                enabled: false
              },
              showInLegend: true
            }
          },
          tooltip: {
            formatter: function() {
                return 'Percentage : '+ this.y+'%' + '</br> Limit : '+ totalCreditLimit;
            }
        },
        series: [
            {
                name: "Percentage",
                colorByPoint: true,
                size: '80%',
                innerSize: '30%',

                data: [
                    {
                        name: "Available",
                        y: availablePercentage,
                        color: "#2dce89"
                    },
                    {
                        name: "Closed",
                        y: 100 - availablePercentage,
                        color: "#cc0033"
                    },
                    {
                        showInLegend: true,
                        name: "Limit",
                        color: "grey"
                       
                    }
                    
                ]
            }
        ],
    }

    return <HighchartsReact
        highcharts={Highcharts}
        options={chartOptions}
    />
        ;


}
export default pie