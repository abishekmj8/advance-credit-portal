import Payment from "@/api/Clients/Payment";
import React, { useEffect,useContext,useRef } from "react";
import {
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Button,
    Card,
    CardHeader,
    Row,
    Col,
    FormGroup,
    Form,
    InputGroup,
} from "reactstrap";
import { ErrorContext } from "@/providers/ErrorBoundary";
import {UserContext} from "@/providers/AuthenticationProvider"
export const SavedCards = ({ setselectedCardDetail }) => {
    const { errorReporting } = useContext(
        ErrorContext
    );
    const {token} = useContext (
        UserContext
    )
    let [selectedCard, setselectedCard] = React.useState("PAYMENT SOURCE");
    let [savedCards, setSavedCards] = React.useState([]);

    const toggleOption = (e, card) => {
        e.preventDefault();
        if (e.target.name == "card") {
            setselectedCard(e.target.innerText)
            setselectedCardDetail(card)
        }
    };

    const getSavedCardDeatails = useRef(async () => {

try{
        const formData = { customerAccountNumber: "AAP001" };
        var cardArray = [];
        var bankArray = []

        const response = await Payment.getSavedCardDetails(formData);
        if (response != undefined && response.status === 200) {
            response.data.content.forEach(element => {
                if (element.instrumentType == "CARD") {
                    cardArray.push(element);

                }
                if (element.instrumentType == "BANK") {
                    bankArray.push(element);

                }



            });
            setSavedCards(response.data.content)
        }
    }
    catch(e){
        console.log(e)
        errorReporting(e,"getSavedCardDeatails Function Fail")
    }

    });

    useEffect(() => {
        getSavedCardDeatails.current()

    }, [token,selectedCard]);

    return (<>
        <FormGroup>
            <InputGroup className="mb-3 w-100">

                <UncontrolledDropdown className="w-100">
                    <DropdownToggle
                        caret
                        color="secondary"
                        id="dropdownMenuButton"
                        type="button"
                        className="w-100 space-pre-wrap"
                    >
                        {selectedCard}
                    </DropdownToggle>

                    <DropdownMenu aria-labelledby="dropdownMenuButton">

                        {savedCards &&
                            savedCards.map((savedCards, i) => (
                                savedCards.instrumentType == "CARD" ?
                                    <DropdownItem  name="card" onClick={(e) => toggleOption(e, savedCards)} key={i}>

                                        {savedCards.cardNumber} ({savedCards.cardExpirationYear}/{savedCards.cardExpirationMonth})
                                    </DropdownItem>
                                    :
                                    <DropdownItem  name="card" onClick={(e) => toggleOption(e, savedCards)} key={i}>

                                        {savedCards.bankAccountNumber} ({savedCards.bankAccountType})
                                    </DropdownItem>
                            ))
                        }


                    </DropdownMenu>

                </UncontrolledDropdown>


            </InputGroup>
        </FormGroup>

    </>);

}
export const FlexForms = () => {
    console.log("FlexForms")

}

