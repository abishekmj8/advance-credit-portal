export const NavbarData = [
    {
        "name": "home",
        "link": "/dashboard/en",
        "type": "single",
        "mobile": true
    },
    {
        "name": "payment",
        "link": "/payment/en",
        "type": "dropdown",
        "list": [
            {
                "name": "make_A_Payment",
                "link": "/payment/submenu1"
            }, {
                "name": "Pending_Payments",
                "link": "/payment/submenu2"
            }, {
                "name": "Payment_History",
                "link": "/payment/submenu1"
            }, {
                "name": "Automatic_Payments",
                "link": "/payment/submenu2"
            }, {
                "name": "Saved_Payment_Cart",
                "link": "/payment/submenu1"
            }, {
                "name": "Payment_Sources",
                "link": "/payment/submenu2"
            }
        ],
        "mobile": true
    },
    {
        "name": "statements",
        "link": "/statement/en",
        "type": "single",
        "mobile": true
    },
    {
        "name": "manage_Account",
        "link": "/statement/en",
        "type": "single",
        "mobile": true
    }
];
export const NavbarRightData = [
    {
        "name": "help",
        "link": "/dashboard/help",
        "type": "single",
        "mobile": true
    },
    {
        "name": "preferences",
        "link": "/dashboard/preferences",
        "type": "single",
        "mobile": true
    },
    {
        "name": "signout",
        "link": "/dashboard/signout",
        "type": "single",
        "mobile": true
    },
    {
        "name": "i_Want_To",
        "link": "/statement/en",
        "type": "dropdown",
        "list": [
            {
                "name": "Dispute_an_Invoice",
                "link": "/payment/submenu1"
            }, {
                "name": "Manage_Payment_Sources",
                "link": "/payment/submenu2"
            }, {
                "name": "Update_My_Profile",
                "link": "/payment/submenu1"
            }, {
                "name": "See_More_Options",
                "link": "/payment/submenu2"
            }
        ],
        "mobile": true
    },
];
export const AdderssData = {
    "firstName": "John",
    "lastName": "Public",
    "address1": "1 Market Street",
    "postalCode": "94105",
    "country": "US",
    "locality": "san francisco",
    "administrativeArea": "CA",
    "email": "flextest@cybs.com",
    "phoneNumber":"9876543120"
}