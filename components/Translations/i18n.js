import i18next from "i18next";
import { initReactI18next } from "react-i18next";
import { TRANSLATIONS_ES } from "../../language/es/language";
import { TRANSLATIONS_EN } from "../../language/en/language";
import LanguageDetector from "i18next-browser-languagedetector"
i18next
.use(LanguageDetector)
  .use(initReactI18next)

  .init({
    fallbackLng : 'en',
    interpolation: {
      escapeValue: false,
    },
    // 'en' | 'es'
    resources: {
      en: {
        translation: TRANSLATIONS_EN
      },
      es: {
        translation: TRANSLATIONS_ES
      }
    },
    debug: false,
  })
  

export default i18next;