import React, { useEffect, useState } from "react";
import Link from "next/link";
import {
    Container,
    Row,
    Col,
    Form,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown
} from "reactstrap";
import {Translate} from "@/components/Translations/translate"
import Router, { useRouter } from 'next/router';
const BreadCrumb = () => {
    let [selectedStore, setselectedStore] = useState("PRIMARY STORE");
    const router = useRouter();
    const languageList = process.env.NEXT_PUBLIC_LANGUAGE;

    const toggleOption = (e) => {
        e.preventDefault();
        if (e.target.name == "store") {
            setselectedStore(e.target.innerText);
        }
    };

    return (
        <Row className="mb-5 m-3">
            <Col xl="6">
                <h6 className="home mb-0">{Translate('home')}\</h6>
                <h3 className="mb-0">{Translate("dashboard")}</h3>
                <h4 className="mb-0">{selectedStore ? selectedStore : Translate("select_store")}</h4>
            </Col>
            <Col xl="6">
                <Form role="form">
                    <div className="mb-3 w-100">
                        <UncontrolledDropdown className="w-100">
                            <DropdownToggle
                                caret
                                color="secondary"
                                id="dropdownMenuButton"
                                type="button"
                                className="w-100 space-pre-wrap"
                            >
                                {selectedStore ? selectedStore : Translate("select_store")}
                            </DropdownToggle>

                            <DropdownMenu aria-labelledby="dropdownMenuButton">
                                <DropdownItem  name="store">
                                    PRIMARY
                                </DropdownItem>
                               
                              
                             
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </div>
                </Form>
            </Col>
        </Row>
    )
}
export default BreadCrumb;