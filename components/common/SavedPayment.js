import React, { useEffect, useState, useContext } from "react";
import { SavedCards } from "@/components/PaymentUtility"
import {
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Button,
    Card,
    CardHeader,
    Row,
    Col,
    FormGroup,
    Form,
    InputGroup,
} from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Accounts from "@/api/Clients/Accounts";
import Router, { useRouter } from 'next/router';
import { Translate } from "@/components/Translations/translate"
import Payment from "@/api/Clients/Payment";
import { ErrorContext } from "@/providers/ErrorBoundary";
const SavedPayment = (props) => {
    const { errorReporting } = useContext(
        ErrorContext
    );
    const [paymentStatus, setPaymentStatus] = useState("PENDING");
    const [loading, setLoading] = useState(false);
    let [selectedCardDetail, setselectedCardDetail] = useState(null);
    let [selectedCard, setselectedCard] = useState("PAYMENT SOURCE");
    let [selectedAmount, setselectedAmount] = useState("PAYMENT AMOUNT");
    let [selectedAmountValue, setselectedAmountValue] = useState(null);
    let [paymentDate, setpaymentDate] = useState("SELECT DATE");
    let [errorMessage, setErrorMessage] = useState(null);

    const toggleOption = (e, value) => {
        e.preventDefault();
        if (e.target.name == "amount") {
            setselectedAmount(e.target.innerText);
            setselectedAmountValue(value)
        }
        if (e.target.name == "card") {
            setselectedCard(e.target.innerText)
        }

    };
    const toggleOptionDate = (e) => {


        let today = new Date();


        setpaymentDate(e.toShortFormat())

    };

    Date.prototype.toShortFormat = function () {

        let monthNames = ["Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec"];

        let day = this.getDate();

        let monthIndex = this.getMonth();
        let monthName = monthNames[monthIndex];

        let year = this.getFullYear();

        return `${day}-${monthName}-${year}`;
    }
    const makePayment = async () => {
        try {
            document.getElementById("loader").style.display = "block";
            let request;

            if (selectedCardDetail.instrumentType == "CARD") {
                request = {
                    "customerAccountNumber": "AAP001",
                    "paymentType": "SAVED_CARD",
                    "tokenDetailsRequest": {
                        "paymentInstrumentId": selectedCardDetail.paymentInstrumentId,
                        "instrumentIdenfier": selectedCardDetail.instrumentIdentifier,
                        "saveCardDetails": true
                    },
                    "orderInformationAmountDetails": {
                        "totalAmount": props.amount,
                        "currency": "USD"
                    },
                    "customerBillinfoRequest": props.Adderss,
                    "invoicesRequest": props.invoicesRequest
                }
            }
            if (selectedCardDetail.instrumentType == "BANK") {
                request = {
                    "customerAccountNumber": "AAP001",
                    "paymentType": "ACH",
                    "tokenDetailsRequest": {
                        "paymentInstrumentId": selectedCardDetail.paymentInstrumentId,
                        "instrumentIdenfier": selectedCardDetail.instrumentIdentifier,
                        "saveCardDetails": false
                    },
                    "orderInformationAmountDetails": {
                        "totalAmount": props.amount,
                        "currency": "USD"
                    },
                    "customerBillinfoRequest": props.Adderss,
                    "invoicesRequest": props.invoicesRequest
                }
            }
            const response = await Payment.makePayment(request)
            if (response && response.status == 200 && response.data.status) {
                setErrorMessage("");
                if (response.data.status == "AUTHORIZED") {
                    props.onSavedPayment("success", response.data)
                }
                if (response.data.status == "DECLINED") {
                    props.onSavedPayment("error", response.data)
                }
            }
            else {
                props.onSavedPayment("fail")
            }
        }
        catch (e) {
            console.log(e);
            errorReporting(e, "makePayment Function Fail")
            props.onSavedPayment("fail")
        }
    }
    const confirmPayment = () => {
        if (selectedCardDetail == null) {
            setErrorMessage(`Select Payment source.`)
        }
        else if (paymentDate == "SELECT DATE") {
            setErrorMessage(`Select Payment Date`)
        }
        else {
            makePayment()
        }
    }
    return (
        <div>
            <Form role="form">
                <SavedCards setselectedCardDetail={setselectedCardDetail} />
                <FormGroup>
                    <InputGroup className="mb-3">

                        {/* <Input placeholder="Payment Date" defaultValue="28-DEC-2021" type="date" /> */}
                        <DatePicker
                            name="payment_date"
                            className="btn w-100 btn-secondary"
                            id="example-datepicker"
                            value={paymentDate}
                            dateFormat="MM/DD/YYYY"
                            onChange={(e) => toggleOptionDate(e)}
                            minDate={new Date()}
                            placeholderText="PAYMENT DATE" />
                    </InputGroup>
                </FormGroup>
                <div id="errors-output" role="alert" className="text-danger text-center">{errorMessage}</div>
                {loading == false &&
                    <div className="text-center">
                        <Button id="payBtn" className="make-payment my-4" color="primary" type="button" onClick={() => confirmPayment()}>
                            {Translate("make_A_Payment")}
                        </Button>


                        <div id="loader" className="loader " style={{ display: "none" }}>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                }
            </Form >
        </div >
    );
}
export default SavedPayment;