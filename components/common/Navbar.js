import React, { useEffect ,useContext} from "react";
import { Translate } from "@/components/Translations/translate"
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Button
} from "reactstrap";
import Router, { useRouter } from 'next/router';
import NavItems from "@/components/common/NavItems";
import NavItemsMobile from "@/components/common/NavItemsMobile";
import NavItemsRightMobile from "@/components/common/NavItemsRightMobile";
import { NavbarData, NavbarRightData } from "@/components/route";
import i18n from "@/components/Translations/i18n";
import { UserContext } from "@/providers/AuthenticationProvider";
const AdminNavbar = () => {
  let [selectedLanguage, setselectedLanguage] = React.useState("English");
  const router = useRouter();
  const languageList = process.env.NEXT_PUBLIC_LANGUAGE;
  const { token, locale, setLocale } = useContext(
    UserContext
  )
  useEffect(() => {
    if (locale) {
    
      i18n.changeLanguage(locale)
      switch (locale) {
        case "en": {
          setselectedLanguage("English");
          break;
        }
        case "es": {
          setselectedLanguage("Spanish");
          break;
        }
        default: {
          setselectedLanguage("English");
          break;
        }
      }
    }
  }, [locale]);

  const toggleOption = (e) => {
    var regex=/\/[a-z]*\//;
    var strToMatch = router.pathname;
    var matched = regex.exec(strToMatch);
    e.preventDefault();
    if (e.target.name == "language" && matched.length >= 1) {
      i18n.changeLanguage(e.target.id)
      setselectedLanguage(e.target.innerText);
      window.location = `${matched[0]}${e.target.id}`

     
    }
  }


  const handleOpen = () => {
    document.getElementById("myNav").style.height = "100%";
  }
  const handleOpenRight = () => {
    document.getElementById("myNavRight").style.height = "100%";
  }

  return (
    <>

      <Navbar className="navbar navbar-expand-lg navbar-horizontal navbar-dark p-0 navbar-sticky" expand="md">
        <Button onClick={() => handleOpen()} className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="p-3 navbar-toggler-icon"></span>
        </Button>
        <Button onClick={() => handleOpenRight()} className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="p-3 p-3 navbar-toggler-icon"><i className="fa-solid fa-circle-chevron-down"></i></span>
        </Button>
        <Container className="px-0 nav-container">
          <NavItemsMobile navbarData={NavbarData} />
          <NavItems navbarData={NavbarData} />
          <NavItemsRightMobile navbarData={NavbarRightData} />
          <div className="right-wing-menu">
            <UncontrolledDropdown className="right-wing-menu">
              <DropdownToggle className="more-options nav-drop-down"
              >
                {Translate("i_Want_To")}
              </DropdownToggle>
              <DropdownMenu end="true">
                <DropdownItem href="#">
                  {Translate("Dispute_an_Invoice")}
                </DropdownItem>
                <DropdownItem href="#">
                  {Translate("Manage_Payment_Sources")}
                </DropdownItem>
                <DropdownItem href="#">
                  {Translate("Update_My_Profile")}
                </DropdownItem>
                <DropdownItem href="#">
                  {Translate("See_More_Options")}
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <UncontrolledDropdown
            >
              <DropdownToggle className="langauge1 nav-drop-down"
              >
                {selectedLanguage}
              </DropdownToggle>
              <DropdownMenu end="true">
                {languageList && JSON.parse(languageList).map((c, i) => (
                  <DropdownItem className="langauge" name="language" key={i} id={c.code} onClick={(e) => toggleOption(e)}>
                    {c.language}
                  </DropdownItem>
                ))
                }

              </DropdownMenu>
            </UncontrolledDropdown>
          </div>
        </Container>
      </Navbar>
    </>
  );
}

export default AdminNavbar;