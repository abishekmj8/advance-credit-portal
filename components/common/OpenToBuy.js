import React, { useEffect, useState } from "react";
import {
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Button,
    Card,
    CardHeader,
    CardBody,
    Row,
    FormGroup,
    Form,
    InputGroup,
    Input
} from "reactstrap";
import Pie from "../charts/pie";
import {Translate} from "@/components/Translations/translate"
import Router, { useRouter } from 'next/router';
const OpenToBuy = ({ availablePercentage,totalCreditLimit }) => {
    console.log("totalCreditLimit",totalCreditLimit)
    const router = useRouter();
    return (
        <Card className="shadow">
            <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                    <div className="col">
                        <div className="col">
                            <h3 className="mb-0">{Translate("open_To_Buy")}</h3>
                        </div>
                    </div>
                </Row>
            </CardHeader>
            <CardBody>
                <Pie 
                totalCreditLimit={Number(totalCreditLimit)}
                availablePercentage={Number(availablePercentage)}></Pie>
            </CardBody>
        </Card>
    )
}
export default OpenToBuy