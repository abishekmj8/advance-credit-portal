import React, { useEffect, useState,useContext } from "react";
import { SavedCards } from "@/components/PaymentUtility"
import {
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Button,
    Input,
    CardHeader,
    Row,
    Col,
    FormGroup,
    Form,
    InputGroup,
} from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Accounts from "@/api/Clients/Accounts";
import Router, { useRouter } from 'next/router';
import { Translate } from "@/components/Translations/translate"
import Payment from "@/api/Clients/Payment";
import {AdderssData} from "@/components/route";
import { ErrorContext } from "@/providers/ErrorBoundary";
import swal from 'sweetalert';
import Link from 'next/link';
import { UserContext } from "@/providers/AuthenticationProvider";
const QuickPayment = ({accountData}) => {
    const [paymentStatus, setPaymentStatus] = useState("PENDING");
    const [loading, setLoading] = useState(false);
    let [selectedCardDetail, setselectedCardDetail] = useState(null);
    let [selectedCard, setselectedCard] = useState("PAYMENT SOURCE");
    let [selectedAmount, setselectedAmount] = useState("PAYMENT AMOUNT");
    let [selectedAmountValue, setselectedAmountValue] = useState(null);
    let [paymentDate, setpaymentDate] = useState(null);
    let [errorMessage, setErrorMessage] = useState(null);
    let [otherFiled, setOtherFiled] = useState(false);
    const { errorReporting } = useContext(
        ErrorContext
    );
    const { token, locale, setLocale } = useContext(
        UserContext
      )
    useEffect(() => {

    }, [])
    const toggleOption = (e, value) => {
        e.preventDefault();
        if (e.target.name == "amount") {
            setOtherFiled(false)
            setselectedAmount(e.target.innerText);
            setselectedAmountValue(value)
        }
        if (e.target.name == "card") {
            setselectedCard(e.target.innerText)
        }
        if (e.target.name == "other") {
            setOtherFiled(true)
            setselectedAmount(e.target.innerText);
            setselectedAmountValue(0)
        }

    };
    const setOtherAmount=(event)=>
    {
        console.log(event.target.value);
        setselectedAmountValue(event.target.value)

    }
    const toggleOptionDate = (e) => {


        let today = new Date();


        setpaymentDate(e.toShortFormat())

    };

    Date.prototype.toShortFormat = function () {

        let monthNames = ["Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec"];

        let day = this.getDate();

        let monthIndex = this.getMonth();
        let monthName = monthNames[monthIndex];

        let year = this.getFullYear();

        return `${day}-${monthName}-${year}`;
    }
    const makePayment = async () => {
        try {
            document.getElementById("loader").style.display = "block";
            document.getElementById("payBtn").style.display = "none";
            let request;

            if (selectedCardDetail.instrumentType == "CARD") {
                request = {
                    "customerAccountNumber": "AAP001",
                    "paymentType": "SAVED_CARD",
                    "tokenDetailsRequest": {
                        "paymentInstrumentId": selectedCardDetail.paymentInstrumentId,
                        "instrumentIdenfier": selectedCardDetail.instrumentIdentifier,
                        "saveCardDetails": true
                    },
                    "orderInformationAmountDetails": {
                        "totalAmount": Number(selectedAmountValue),
                        "currency": "USD"
                    },
                    "customerBillinfoRequest": AdderssData
                }
            }
            if (selectedCardDetail.instrumentType == "BANK") {
                request = {
                    "customerAccountNumber": "AAP001",
                    "paymentType": "ACH",
                    "tokenDetailsRequest": {
                        "paymentInstrumentId": selectedCardDetail.paymentInstrumentId,
                        "instrumentIdenfier": selectedCardDetail.instrumentIdentifier,
                        "saveCardDetails": false
                    },
                    "orderInformationAmountDetails": {
                        "totalAmount": selectedAmountValue,
                        "currency": "USD"
                    },
                    "customerBillinfoRequest": AdderssData
                }
            }
            const response = await Payment.makePayment(request)
            if (response && response.status == 200 && response.data.status) {
            
                if (response.data.status == "AUTHORIZED") {
                    setErrorMessage("Payment Success");
                    document.getElementById("loader").style.display = "none";
                    swal({
                        title: "Payment Success",
                        text: `Transaction Id : ${response.data.id}`,
                        icon: "success",
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                            document.getElementById("payBtn").style.display = "inline-block";
                        }
                      });
                }
                else if (response.data.status == "DECLINED") {
                    // setPaymentStatus(response.data.status);
                    setErrorMessage(response.data.errorMessage);
                    document.getElementById("loader").style.display = "none";
                    swal({
                        title: "Payment Failed",
                        text: response.data.errorMessage,
                        icon: "error",
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                            document.getElementById("payBtn").style.display = "inline-block";
                        }
                      });
                }
                else {
                    setErrorMessage(response.data.errorMessage);
                    document.getElementById("loader").style.display = "none";
                    swal({
                        title: "Payment Failed",
                        text: response.data.errorMessage,
                        icon: "error",
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                            document.getElementById("payBtn").style.display = "inline-block";
                        }
                      });
                }
            }
            else {
                setErrorMessage("Someting went Wrong!");
                swal({
                    title: "Someting went Wrong!",
                    text: `Please try again later.`,
                    icon: "error",
                  })
                document.getElementById("loader").style.display = "none";
                document.getElementById("payBtn").style.display = "inline-block";
            }
        }
        catch (e) {
            errorReporting(e,"makePayment Fail")
            swal({
                title: "Someting went Wrong!",
                text: `Please try again later.`,
                icon: "error",
              })
            document.getElementById("loader").style.display = "none";
            document.getElementById("payBtn").style.display = "inline-block";
            console.log(e);
        }
    }
    const confirmPayment = () => {
        if (selectedAmountValue == null && selectedCardDetail == null) {
            swal({
                text: `Select Payment amount and source.`,
                icon: "error",
            })
        }
        else if (selectedAmountValue !== null && selectedCardDetail == null) {
            swal({
                text: `Select Payment source`,
                icon: "error",
            })
        }
        else if (selectedAmount == "Other" && selectedAmountValue == 0) {
            swal({
                    text: `Please enter amount`,
                    icon: "error",
                })
        }
        else if (selectedAmountValue == null && selectedCardDetail !== null) {
            swal({
                text: `Select Payment amount.`,
                icon: "error",
            })
        }
        else {
            swal({
                title: "Confirm Payment",
                text: "Do you want to continue?",
                // icon: "info",
                buttons: {
                    cancel: "NO",
                    catch: "YES"
                },
                dangerMode: true,
            })
                .then((willAccept) => {
                    if (willAccept) {
                        makePayment()
                    }
                });
        }
    }
    return (
        <div>
            <h5 className="ml-4">{Translate("Quick_Payment")}</h5>
            <Form role="form">
                <FormGroup>
                    <InputGroup className="mb-3 w-100">

                        {/* <Input value="GOODYEAR TIRE CENTER" type="text" /> */}
                        <UncontrolledDropdown className="w-100">
                            <DropdownToggle
                                caret
                                color="secondary"
                                id="dropdownMenuButton"
                                type="button"
                                className="w-100 space-pre-wrap"
                            >
                                {selectedAmount}
                            </DropdownToggle>

                            <DropdownMenu aria-labelledby="dropdownMenuButton">
                                {accountData &&
                                    <DropdownItem href="" name="amount" onClick={(e) => toggleOption(e, 3782.56)} >
                                        ${Number(accountData.totalCreditLimit) - Number(accountData.totalAvailableCredit)} (Current Balance)
                                    </DropdownItem>
                                }
                                {accountData &&
                                    <DropdownItem href="" name="amount" onClick={(e) => toggleOption(e, 5782.56)} >
                                        ${accountData.lastStatementDueAmount} (Balance Due)
                                    </DropdownItem>
                                }
                                <DropdownItem href="" name="other" onClick={(e) => toggleOption(e, "other")} >
                                 Other
                                </DropdownItem>
                            </DropdownMenu>
                   {otherFiled&&
                            <InputGroup className="mt-3">
<Input type="number" placeholder="Enter The Amount" name="other_amount" id="other_amount" className="" onChange={(e)=>setOtherAmount(e)}></Input>
</InputGroup>
}

                        </UncontrolledDropdown>


                    </InputGroup>
                </FormGroup>
                <SavedCards setselectedCardDetail={setselectedCardDetail} />
                <FormGroup>
                    <InputGroup className="mb-3">

                        {/* <Input placeholder="Payment Date" defaultValue="28-DEC-2021" type="date" /> */}
                        <DatePicker
                            name="payment_date"
                            className="btn w-100 btn-secondary"
                            id="example-datepicker"
                            value={paymentDate}
                            dateFormat="MM/DD/YYYY"
                            onChange={(e) => toggleOptionDate(e)} 
                            minDate={new Date()}
                            placeholderText="PAYMENT DATE"
                            />

                    </InputGroup>
                </FormGroup>
                {loading == false &&
                    <div className="text-center">
                        <Button id="payBtn" className="make-payment my-4" color="primary" type="button" onClick={() => confirmPayment()}>
                            {Translate("make_A_Payment")}
                        </Button>
                       

                        <div id="loader" className="loader " style={{ display: "none" }}>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                }
                 <Link href={`/invoice/${locale}`}>
                 <a className="ml-7" href="#">{Translate("select_invoice")}</a>
                 </Link>
            </Form >

            <a href="#"><h6 className="text-right  mt-3" ><u>{Translate("setup_Auto_Payment")}</u></h6></a>
        </div >
    );
}
export default QuickPayment;