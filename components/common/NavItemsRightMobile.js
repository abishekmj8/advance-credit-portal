import React, { useEffect } from "react";
import Link from "next/link";
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Navbar,
    NavItem,
    NavLink,
    Nav,
    Container,
    Button
} from "reactstrap";
import Collapsible from 'react-collapsible';
import {Translate} from "@/components/Translations/translate";
import i18n from "@/components/Translations/i18n";
import Router, { useRouter } from 'next/router';
const NavItemsRightMobile = ({ navbarData }) => {
    let [selectedLanguage, setselectedLanguage] = React.useState("English");
    const router = useRouter();
    const languageList = process.env.NEXT_PUBLIC_LANGUAGE;
    const toggleOption = (e) => {
          i18n.changeLanguage(e.code)
          setselectedLanguage(e.language);
          Router.push(`/dashboard/${e.code}`)
      }
    const handleClose = () => {
        document.getElementById("myNavRight").style.height = "0%";
    }
    return (
        <div id="myNavRight" className="overlay">
            <a className="closebtn" onClick={() => handleClose()}>&times;</a>
            <div className="overlay-content">
                {navbarData && navbarData.map((data, index) => (
                    data.type == "single" && data.mobile == true?
                            <Link href={data.link} passHref key={index}>
                                {Translate(data.name)}
                            </Link>
                        : data.type == "dropdown" && data.mobile == true &&
                        <a key={index}>
                            <Collapsible trigger={Translate(data.name)}>
                                {data.list && data.list.map((opt, i) => (
                                    <li className="nav-item w-95 row ml-3 mt-2" key={i}>
                                        <Link href={opt.link} passHref>
                                        <label className="d-block m-0 w-100">
                                            <span className="small inline-block w-53"> {Translate(opt.name)}</span>
                                        </label>
                                        </Link>
                                    </li>
                                ))}
                            </Collapsible>
                        </a>
                ))}
                <a>
                            <Collapsible className="text-center" trigger={selectedLanguage} >
                            {languageList && JSON.parse(languageList).map((c, i) => (
                                    <li className="nav-item w-95 row  mt-2" name="language" key={i} id={c.code} onClick={() => toggleOption(c)}>
                                        <label className="d-block m-0 w-100">
                                            <span className="small inline-block w-53"> {c.language}</span>
                                        </label>
                                    </li>
                                ))}
                            </Collapsible>
                        </a>
            </div>
        </div>
    )
}
export default NavItemsRightMobile;