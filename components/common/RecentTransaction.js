import React, { useEffect ,useContext,useRef} from "react";
import {
    Card,
    CardHeader,
    Row,
    Col
} from "reactstrap"; 3
import Grid from "@/components/grid/grid";
import Transactions from "@/api/Clients/Transactions";
import { Translate } from "@/components/Translations/translate"
import Router, { useRouter } from 'next/router';
import InvoiceModal from "@/components/modal/invoiceModal";
import CreditMemoModal from "@/components/modal/creditMemoModal";
import { ErrorContext } from "@/providers/ErrorBoundary";
import PaymentTransactionModal from "@/components/modal/paymentTransactionModal";
const RecentTransaction = () => {
   
    // console.log(router.asPath)

    const { errorReporting } = useContext(
        ErrorContext
    );
    const [modalOpen, setModalOpen] = React.useState(false);
    const [modalType, setModalType] = React.useState("");
    const [modalData, setModalData] = React.useState(null)
    const [tansactionDetails, setTransactionDetails] = React.useState([])
    const [tansactionActivity, setTransactionActivity] = React.useState([])
    useEffect(() => {
        recentTransactions.current()
    }, []);

    let [recentTransaction, setRecentTransaction] = React.useState([]);
    const recentTransactions = useRef(async () => {
        try {
            let accountNumber = "21001";
            if (accountNumber) {
                const response = await Transactions.recentTransactions(accountNumber)

                if (response.status == 200) {
                    setRecentTransaction(response.data.content)
                }
                else {
                    setRecentTransaction(null)

                }

            }


        }
        catch (e) {
            errorReporting(e,"recentTransactions Function Fail")
            console.log(e)
        }

    });
    const switchModal = (data) => {
        setModalType(data.transactionType)
        if (data.transactionType == "Invoice") {
            invoiceDetails(data.transactionId)
            setModalData(data)
        }
        if (data.transactionType == "Credit-Memo") {
            invoiceDetails(data.transactionId)
            setModalData(data)
        }
        if (data.transactionType == "Payment") {
            paymentDetails(data.transactionId)
            // setModalData(data)
        }
    }
    const invoiceDetails = async (id) => {
        try {
            if (id) {
                const response = await Transactions.invoiceDetails(id)
                const activity =await Transactions.invoiceTransactionActivity(id)
                if (response.status == 200) {
                    setTransactionDetails(response.data)
                    setModalOpen(true)
                }
                 if (activity.status == 200) {
                     setTransactionActivity(activity.data.content)
                 }
            }
        }
        catch (e) {
            errorReporting(e,"invoiceDetails Function Fail")
            console.log(e)
        }
    }
    const paymentDetails = async (id) => {
        try {
            if (id) {
                const response = await Transactions.paymentDetails(id);
                const headerResponse = await Transactions.paymentHeaderDetails(id);
                console.log(headerResponse.data.content)
                if (response.status == 200) {
                    setTransactionDetails(response.data)
                }
                if(headerResponse.status == 200) {
                    setModalData(headerResponse.data.content[0])
                    setModalOpen(true)
                }
            }
        }
        catch (e) {
            errorReporting(e,"invoiceDetails Function Fail")
            console.log(e)
        }
    }
    return (  <>

        <Card className="shadow margin-top-row-1">
            <CardHeader className="border-0">
                <Row className="align-items-center">
                    <div className="col">
                        <h3 className="mb-0">{Translate("recent_Transaction")}</h3>
                        {/* <h3 className="mb-0">Recent Transaction</h3> */}
                    </div>

                </Row>
            </CardHeader>
            </Card>

        <Card className="shadow margin-top-row-1">
            <Row className="align-items-center">
                <Col className="mb-5 mb-xl-0" xl="12">
                    <Grid data={recentTransaction} switchModal={switchModal} />
                </Col>
            </Row>
            {modalType == "Invoice" &&
                <InvoiceModal modalData={modalData} toggle={() => setModalOpen(!modalOpen)} modalOpen={modalOpen} tansactionDetails={tansactionDetails} tansactionActivity={tansactionActivity}/>
            }
            {modalType == "Credit-Memo" &&
                <CreditMemoModal modalData={modalData} toggle={() => setModalOpen(!modalOpen)} modalOpen={modalOpen} tansactionDetails={tansactionDetails}/>
            }
            {modalType =="Payment" &&
                <PaymentTransactionModal modalData={modalData} toggle={() => setModalOpen(!modalOpen)} modalOpen={modalOpen} tansactionDetails={tansactionDetails}/>
            }
        </Card>
      </>
    )
}
export default RecentTransaction;