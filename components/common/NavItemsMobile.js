import React, { useEffect } from "react";
import Link from "next/link";
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Navbar,
    NavItem,
    NavLink,
    Nav,
    Container,
    Button
} from "reactstrap";
import Collapsible from 'react-collapsible';
import {Translate} from "@/components/Translations/translate"
const NavItems = ({ navbarData }) => {

    const handleClose = () => {
        document.getElementById("myNav").style.height = "0%";
    }
    return (
        <div id="myNav" className="overlay">
            <a className="closebtn" onClick={() => handleClose()}>&times;</a>
            <div className="overlay-content">
                {navbarData && navbarData.map((data, index) => (
                    data.type == "single" && data.mobile == true?
                            <Link href={data.link} passHref key={index}>
                                {Translate(data.name)}
                            </Link>
                        : data.type == "dropdown" && data.mobile == true &&
                        <a key={index}>
                            <Collapsible trigger={Translate(data.name)}>
                                {data.list && data.list.map((opt, i) => (
                                    <li className="nav-item w-95 row ml-3 mt-2" key={i}>
                                        <Link href={opt.link} passHref>
                                        <label className="d-block m-0 w-100">
                                            <span className="small inline-block w-53"> {Translate(opt.name)}</span>
                                        </label>
                                        </Link>
                                    </li>
                                ))}
                            </Collapsible>
                        </a>
                ))}
            </div>
        </div>
    )
}
export default NavItems;