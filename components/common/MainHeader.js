import React, { useEffect } from "react";
import Link from "next/link";
import { Translate } from "@/components/Translations/translate"
import Router, { useRouter } from 'next/router';
import Image from "next/image";
import cookie from "js-cookie";
import Logo from "@/components/common/logo";
// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Navbar,
  Nav,
  Container,
  Media,
} from "reactstrap";
const MainHeader = (props) => {
  const router = useRouter();
  const signout = () => {
    cookie.remove("aap-auth-token")
    router.push("/login")
  }
  return (
    <>
      <Navbar className="navbar-top navbar-dark header-background" expand="md" id="navbar-main">
        <Container fluid>

          <div className={'d-flex align-items-center header-width'}>
            {/* <Image alt={props.logo.imgAlt} width='158' height='48' className="navbar-brand-img" src={props.logo.imgSrc} /> */}
              <Logo/>
            <Link href="/admin/dashboard">
              <a className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block px-2">
                {props.brandText}
              </a>
            </Link>
          </div>


          <Nav className="align-items-center d-none d-md-flex" navbar>
            <UncontrolledDropdown nav>

              <Media className="align-items-center">

                <Media className="mr-2 ml-2 d-none d-lg-block">
                  <span className="mb-0 text-sm text-white font-weight-bold">
                    {/* <i className="ni ni-support-16 text-white px-2"></i> */}

                    {Translate("help")}
                  </span>
                </Media>

                <Media className="mr-2 ml-2 d-none d-lg-block align-items-center">

                  <span className="mb-0 text-sm text-white font-weight-bold">
                    {/* <i className="ni ni-settings-gear-65 text-white px-2"></i> */}
                    {Translate("preferences")}
                  </span>
                </Media>

                <Media className="mr-2 ml-2 d-none d-lg-block" >

                  <a onClick={() => signout()}>
                    <span className="mb-0 text-sm text-white font-weight-bold" style={{cursor: "pointer"}}>
                      {/* <i className=" ni ni-bold-right text-white px-2"></i> */}
                      {Translate("signout")}
                    </span></a>
                </Media>
              </Media>

            </UncontrolledDropdown>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
}

export default MainHeader;