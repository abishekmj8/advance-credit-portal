import React, { useEffect } from "react";
import Link from "next/link";
import {
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Navbar,
    NavItem,
    NavLink,
    Nav,
    Container,
    Button
} from "reactstrap";
import {Translate} from "@/components/Translations/translate"
const NavItems = ({ navbarData }) => {
    return (
        <Nav className="mr-auto collapse navbar-collapse" navbar>
            {navbarData && navbarData.map((data, index) => (
                data.type == "single" ?
                        <NavItem key={index}>
                            <Link href={data.link} passHref>
                                <NavLink  className="nav-link-icon">
                                    <span className="home nav-link-inner--text">{Translate(data.name)}</span>
                                </NavLink>
                            </Link>
                        </NavItem>
                     : data.type == "dropdown" &&
                    <UncontrolledDropdown
                        inNavbar
                        nav
                        key={index}
                    >
                        <DropdownToggle
                            caret
                            nav
                            className="payment"
                        >
                            {Translate(data.name)}
                        </DropdownToggle>
                        <DropdownMenu end="true">
                            {data.list && data.list.map((opt, i) => (
                                <DropdownItem href={opt.link} key={i}>
                                    {Translate(opt.name)}
                                </DropdownItem>
                            ))}
                        </DropdownMenu>
                    </UncontrolledDropdown>
            ))}
        </Nav>
    )
}
export default NavItems;