import React, { useEffect ,useContext,useRef} from "react";
import {SavedCards} from "@/components/PaymentUtility"
import {
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Button,
    Card,
    CardHeader,
    Row,
    Col,
    FormGroup,
    Form,
    InputGroup,
} from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Accounts from "@/api/Clients/Accounts";
import Router, { useRouter } from 'next/router';
import {Translate} from "@/components/Translations/translate"
import Payment from "@/api/Clients/Payment";
import QuickPayment from "@/components/common/QuickPayment";
import { ErrorContext } from "@/providers/ErrorBoundary";


const AccountSummary = ({ setAvailablePercentage,setTotalCreditLimit }) => {
    const { errorReporting } = useContext(
        ErrorContext
    );
    const router = useRouter();
    const languageList = process.env.NEXT_PUBLIC_LANGUAGE;
    useEffect(() => {
        accountSummaryDetails.current()

    }, []);
    let [accountData, setAccountData] = React.useState(null);

    const accountSummaryDetails = useRef(async () => {
        try {
            let accountNumber="2104";
            const response = await Accounts.accountSummary(accountNumber)
            if (response.status == 200) {
                setAccountData(response.data)
                setAvailablePercentage(response.data.totalAvailableCreditPercentage)
     
                setTotalCreditLimit(response.data.totalCreditLimit)
            }
        }
        catch (e) {
            console.log(e)
            errorReporting(e,"accountSummary Function Fail")
        }
    
    });
    return (
        <Card className="shadow">
            <CardHeader className="border-0">
                <Row className="align-items-center">
                    <div className="col">
                        <h3 className="mb-0">{Translate("account_Summary")}</h3>
                    </div>
                    <div className="col text-right">

                    </div>
                </Row>
            </CardHeader>
            <Row className="align-items-center">
                <Col xl="5">
                    <h5 className="ml-4 text-uppercase">{Translate("account_Summary")}</h5>
                    <h1 className="ml-4">{accountData ? accountData.totalAvailableCredit : "0000.00"}</h1>
                    <h5 className="ml-4">{Translate("statement_Balance")}: {accountData ? accountData.totalAmountDueRemaining : "0000"}</h5>
                    <h5 className="ml-4"><u>{accountData ? accountData.lastStatementDueDate : "XX-XXX-XX"} Statement</u></h5>
                    <h5 className="ml-4 text-uppercase"> {Translate("balance_Due")}</h5>
                    <h1 className="ml-4">{accountData ? accountData.lastStatementDueAmount : "0000"}</h1>
                    <h5 className="ml-4"> {Translate("payment_Due")}</h5>
                    <h5 className="ml-4 text-uppercase"> {Translate("last_Payment")}</h5>
                    <h1 className="ml-4">{accountData ? accountData.lastPaymentAmount : "0000.00"}</h1>
                    <h5 className="ml-4"> {Translate("payment_Date")}: {accountData ? accountData.lastPaymentDate : "XX-XXX-XX"}</h5>

                    <Row className="col mt-4 ml-2 row">

                        {/* <i className="ni ni-credit-card pr-2" /> */}
                        <h6 className="" > {Translate("no_Scheduled_Auto_Payment")}</h6>
                    </Row>
                </Col>
                <Col xl="6" className="mx-3">
                   <QuickPayment accountData={accountData}/>
                </Col>
            </Row>
        </Card>
    )
}
export default AccountSummary;