import React, { useEffect, useState, useContext } from "react";
import {
    Row,
    Col,
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    Container,
    Card,
    CardHeader,
    CardBody,
    NavItem,
    NavLink,
    Nav,
    Progress,
    Table,
    FormGroup,
    Form,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Label,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown
} from "reactstrap";
import Payment from "@/api/Clients/Payment";
import SavedPayment from "../common/SavedPayment";
import { AdderssData } from "@/components/route";
import { ErrorContext } from "@/providers/ErrorBoundary";

const PaymentModal = (props) => {
    const { errorReporting } = useContext(
        ErrorContext
    );
    const [state, updateState] = useState({
        form: {
            mainTab: "payment",
            paymentStep: "savedCard",
            accType: "C",
            accTypeName: "Checking",
            isAchPayment: false,
            isChecked: false,
            isError: false,
            expYear: 2022,
            expMonth: "01",
            phone: "",
            capture: null,
            AddressError: false,
        },
    })
    const [Address, setAddress] = useState({
        Addressform: {
            firstName: AdderssData.firstName,
            lastName: AdderssData.lastName,
            address1: AdderssData.address1,
            postalCode: AdderssData.postalCode,
            country: AdderssData.country,
            locality: AdderssData.locality,
            administrativeArea: AdderssData.administrativeArea,
            email: AdderssData.email,
            phoneNumber: AdderssData.phoneNumber
        },
    })
    const updateForm = (key, value) => {
        const { form } = state
        form[key] = value
        updateState({
            ...state,
            form,
        })
    }
    const updateAddress = (key, value) => {
        const { Addressform } = Address
        Addressform[key] = value
        setAddress({
            ...Address,
            Addressform,
        })
    };
    const switchTab = (tab) => {
        updateForm("paymentStep", tab)
        updateForm("paymentType", tab)
        if (tab == "card") {
            updateForm("paymentLoading", true)
            generateKey();
        }
        if (tab == "ach") {
            updateForm("isError", false)
        }

    }
    /*NEW PAYMENT FUNCTIONS START*/
    const toggleOption = (e) => {
        if (e.target.name == "accType") {
            if (e.target.outerText == "Checking") {
                updateForm(e.target.name, "C")
                updateForm("accTypeName", e.target.outerText)
            }
            else {
                updateForm(e.target.name, "S")
                updateForm("accTypeName", e.target.outerText)
            }
        }
        else {
            updateForm(e.target.name, e.target.outerText)
        }
    };
    const generateKey = async () => {
        try {
            const pkeyReq = await Payment.getPublicKey();
            updateForm("capture", pkeyReq.data.keyId)
            updateForm("paymentLoading", false)
            flexTokenGenrate(pkeyReq.data.keyId);
        }
        catch (e) {
            console.log(e)
            errorReporting(e, "generateKey Function Fail")
            //throw paymentError();
        }
    }
    const paymentError = () => {
        updateForm("paymentStep", "error")
    }
    const flexTokenGenrate = (captureContext) => {
        let callback;
        var flex = new Flex(captureContext);
        var custom = {
            'input': {
                'font-size': '14px',
                'font-family': 'helvetica, tahoma, calibri, sans-serif',
                'color': '#555',
                'background': 'red'
            },
            ':focus': { 'color': 'blue' },
            ':disabled': { 'cursor': 'not-allowed' },
            'valid': { 'color': '#3c763d' },
            'invalid': { 'color': '#a94442' }
        };
        var microform = flex.microform({ styles: custom });
        var number = microform.createField('number', { placeholder: 'Enter card number', maxLength: '16', styles: custom });
        var securityCode = microform.createField('securityCode', { placeholder: '•••', inputType: "password" });
        number.load('#cardNumber');
        securityCode.load('#securityCode-container');


        //*************Click on Pay Button******************/


        var payButton = document.querySelector('#pay-button');
        payButton.addEventListener('click', async () => {
            var errorsOutput = document.querySelector('#errors-output');
            var options = {
                expirationMonth: state.form.expMonth,
                expirationYear: state.form.expYear
            };
            //**********************start promise**************/
            callback = new Promise((resolve, reject) => {
                microform.createToken(options, function (err, token) {

                    //************************* handle error******************/
                    if (err) {
                        errorsOutput.textContent = err.message;
                        reject(err.message)
                        //throw this.paymentError();
                    } else {
                        //******/ At this point you may pass the token back to your server as you wish.
                        // In this example we append a hidden input to the form and submit it. ********//


                        var flexResponse = token;



                        if (flexResponse) {

                            resolve(flexResponse);


                        }
                        else {
                            reject("No or Invalid Flex response")
                        }
                    }
                });
            });//********************************Promose end***************************/

            callback.then(
                function (value) { confirmPay(value) },
                function (error) { console.log(error) }
            )
        });

    }
    //*****************************************Payment confirmation*************************/
    const confirmPay = (TransientToken) => {
        updateForm("FlexToken", TransientToken)
        updateForm("paymentStep", "confirm")
    }
    /******************************Final Payment to cybersource *************************************/
    const cardPayNow = async () => {
        try {
            let paymentRequest;
            let request = {
                "customerAccountNumber": "AAP001",
                "paymentType": "TRANSIENT_TOKEN",
                "tokenDetailsRequest": {
                    "transientToken": state.form.FlexToken
                },
                "orderInformationAmountDetails": {
                    "totalAmount": props.amount,
                    "currency": "USD"
                },
                "customerBillinfoRequest": Address.Addressform,
                "invoicesRequest": props.invoicesRequest
            }
            paymentRequest = await Payment.makePayment(request);
            //***********************************if payment is sucess*******************************/
            if (paymentRequest.status == 200) {
                if (paymentRequest.data.status == "AUTHORIZED") {
                    updateForm("mainTab", "success")
                    updateForm("transactionID", paymentRequest.data.id)
                    updateForm("success", 1)
                    updateForm("responseMessage", "Payment Successful")
                    //********************************if payment failed case 3********************************************/
                }
                if (paymentRequest.data.status == "DECLINED") {
                    updateForm("mainTab", "success")
                    updateForm("success", 3)
                    updateForm("responseMessage", paymentRequest.data.errorMessage)
                }
            }
            else {
                updateForm("mainTab", "success")
                updateForm("success", 3)
                updateForm("responseMessage", "Payment Failed. Please try again after sometime.")
            }
        }
        catch (e) {
            console.log(e)
            errorReporting(e, "cardPayNow Function Fail")

            //throw this.paymentError();
        }
    }
    /*NEW PAYMENT FUNCTIONS END*/

    /*ACH PAYMENT FUNCTIONS START*/
    const achUpdate = (e) => {
        if (e.target.name == 'accNo') {
            updateForm("accNo", e.target.value)
        } else {
            updateForm("routingNumber", e.target.value)
        }
    };
    const isChecked = () => {
        updateForm("isChecked", !state.form.isChecked)
    }
    const BankPayNow = () => {
        if (state.form.accNo && state.form.accType && state.form.routingNumber) {
            updateForm("paymentStep", "confirm")
        }
        else {
            updateForm("isError", true)
        }
    }
    /******************************Final Payment to cybersource *************************************/
    const achPayNow = async () => {
        try {

            // var paymentData = new FormData();
            // paymentData.append("bankAccountNumber", state.form.accNo);
            // paymentData.append("bankAccountType", state.form.accType);
            // paymentData.append("bankRoutingNumber", state.form.routingNumber);
            let paymentRequest;
            paymentRequest = {
                "customerAccountNumber": "AAP001",
                "bankPaymentRequest": {
                    "bankAccountType": state.form.accType,
                    "bankAccountNumber": state.form.accNo,
                    "bankRoutingNumber": state.form.routingNumber,
                    "saveBankDetails": state.form.isChecked
                },
                "orderInformationAmountDetails": {
                    "totalAmount": "200",
                    "currency": "USD"
                },
                "customerBillinfoRequest": Address.Addressform,
            }

            paymentRequest = await Payment.makePayment(paymentRequest);

            //***********************************if payment is sucess*******************************/
            if (paymentRequest.status == 200) {
                updateForm("showJsonData", JSON.stringify(paymentRequest.data, null, 2))
                updateForm("isError", false)
                updateForm("mainTab", "success")
                updateForm("transactionID", paymentRequest.data.id)
                updateForm("success", 1)
                updateForm("responseMessage", "Payment Successful")
            }

        }
        catch (e) {
            console.log(e)
            errorReporting(e, "achPayNow Function Fail")
            //throw this.paymentError();
        }
    }

    const PayNow = () => {
        let validate;
        Object.values(Address.Addressform).forEach(element => {
            if (element.length == 0) {
                validate = false;
                updateForm("AddressError", true)
                updateForm("responseMessage", "One or more Address fields is missing")
            }
            else if (Address.Addressform.phoneNumber.length != 10) {
                validate = false;
                updateForm("AddressError", true)
                updateForm("responseMessage", "Invalid Phone number in Address")
            }
            else {
                validate = true;
            }
        });
        if (validate == true) {
            if (state.form.paymentType == "card") {
                cardPayNow()
            }
            if (state.form.paymentType == "ach") {
                achPayNow()
            }
        }
    }
    const onSavedPayment = (status, response) => {
        console.log(status, response)
        if (status == "success") {
            updateForm("responseMessage", "Payment Successful")
            updateForm("transactionID", response.id)
            updateForm("success", 1)
        }
        if (status == "error") {
            updateForm("responseMessage", response.errorMessage)
            updateForm("success", 3)
        }
        if (status == "fail") {
            updateForm("responseMessage", "Someting went Wrong!")
            updateForm("success", 3)
        }
        updateForm("mainTab", "success")
    }
    function validateAddress() {
        Object.values(Address.Addressform).forEach(element => {
            if (element.length == 0) {
                return false;
            }
            return true;
        });
    }
    return (
        <Modal toggle={() => props.toggle()} isOpen={props.modalOpen} onExit={() => props.modalClose()} size="lg" className="modal-size modal-xl">
            <div className=" modal-header w-90">
                <h3 className=" modal-title" id="exampleModalLabel">
                    Payment
                </h3>
                <button
                    aria-label="Close"
                    className=" close"
                    type="button"
                    onClick={() => props.modalClose()}
                >
                    <span aria-hidden={true}>×</span>
                </button>
            </div>
            <ModalBody >
                <Container fluid>
                    {state.form.mainTab == "payment" &&
                        <Row>
                            {/* Address div start */}
                            <Col xl="6">
                                <Card className="shadow">
                                    <CardHeader>
                                        <h3 className="text-left">Billing Address</h3>
                                    </CardHeader>
                                    <CardBody className="align-items-center">
                                        <Form role="form">
                                            <FormGroup>
                                                <Label for="expMonth">
                                                    First Name
                                                </Label>
                                                <InputGroup className="mb-3 w-100">
                                                    <Input placeholder="First Name" id="firstName" name="firstName" type="text" value={Address.Addressform.firstName} onChange={(e) => updateAddress(e.target.name, e.target.value)} />
                                                </InputGroup>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label for="expMonth">
                                                    Last Name
                                                </Label>
                                                <InputGroup className="mb-3 w-100">
                                                    <Input placeholder="Last Name" id="lastName" name="lastName" type="text" value={Address.Addressform.lastName} onChange={(e) => updateAddress(e.target.name, e.target.value)} />
                                                </InputGroup>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label for="expMonth">
                                                    Address 1
                                                </Label>
                                                <InputGroup className="mb-3 w-100">
                                                    <Input placeholder="Address1" id="address1" name="address1" type="text" value={Address.Addressform.address1} onChange={(e) => updateAddress(e.target.name, e.target.value)} />
                                                </InputGroup>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label for="expMonth">
                                                    Postal Code
                                                </Label>
                                                <InputGroup className="mb-3 w-100">
                                                    <Input placeholder="Postal Code" id="postalCode" name="postalCode" type="text" value={Address.Addressform.postalCode} onChange={(e) => updateAddress(e.target.name, e.target.value)} />
                                                </InputGroup>
                                            </FormGroup>

                                            <FormGroup>
                                                <Label for="expMonth">
                                                    Country
                                                </Label>
                                                <InputGroup className="mb-3 w-100">
                                                    <Input placeholder="Country" id="country" name="country" type="text" value={Address.Addressform.country} onChange={(e) => updateAddress(e.target.name, e.target.value)} />
                                                </InputGroup>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label for="expMonth">
                                                    Locality
                                                </Label>
                                                <InputGroup className="mb-3 w-100">
                                                    <Input placeholder="Locality" id="locality" name="locality" type="text" value={Address.Addressform.locality} onChange={(e) => updateAddress(e.target.name, e.target.value)} />
                                                </InputGroup>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label for="administrativeArea">
                                                    Administrative Area
                                                </Label>
                                                <InputGroup className="mb-3 w-100">
                                                    <Input placeholder="Administrative Area" id="administrativeArea" name="administrativeArea" type="text" value={Address.Addressform.administrativeArea} onChange={(e) => updateAddress(e.target.name, e.target.value)} />
                                                </InputGroup>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label for="email">
                                                    Email
                                                </Label>
                                                <InputGroup className="mb-3 w-100">
                                                    <Input placeholder="email" id="email" type="email" name="email" value={Address.Addressform.email} onChange={(e) => updateAddress(e.target.name, e.target.value)} />
                                                </InputGroup>
                                            </FormGroup>

                                            <FormGroup>
                                                <Label for="phone">
                                                    Phone
                                                </Label>
                                                <InputGroup className="mb-3 w-100">
                                                    <Input placeholder="Phone" id="phone" type="text" name="phoneNumber" value={Address.Addressform.phoneNumber} onChange={(e) => updateAddress(e.target.name, e.target.value)} />
                                                </InputGroup>
                                            </FormGroup>
                                        </Form>
                                    </CardBody>
                                </Card>
                            </Col>
                            {/* Address div end */}
                            {/* Payment div start */}
                            <Col xl="6">
                                <Card className="shadow">
                                    <CardHeader>
                                        <Row className="mb-2">

                                            <Col xl="4" className="px-0" >
                                                <div className="w-100 dropdown">
                                                    <button type="button" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false" className={state.form.paymentStep == "savedCard" ? "w-100 btn btn-secondary active" : "w-100 btn btn-secondary"} onClick={() => switchTab("savedCard")}>
                                                        Saved Card/bank
                                                    </button>
                                                </div>
                                            </Col>
                                            <Col xl="4" className="px-0" >
                                                <div className="w-100 dropdown">
                                                    <button type="button" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false" className={state.form.paymentStep == "card" ? "w-100 btn btn-secondary active" : "w-100 btn btn-secondary"} onClick={() => switchTab("card")}>
                                                        Card Payment
                                                    </button>
                                                </div>
                                            </Col>
                                            <Col xl="4" className="px-0" >
                                                <div className="w-100 dropdown">
                                                    <button type="button" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false" className={state.form.paymentStep == "ach" ? "w-100 btn btn-secondary active" : "w-100 btn btn-secondary"} onClick={() => switchTab("ach")}>
                                                        ACH Payment
                                                    </button>
                                                </div>
                                            </Col>
                                        </Row>
                                    </CardHeader>
                                    <CardBody className="align-items-center">
                                        <Row>
                                            <Col xl="12" className="mt-3">
                                                {state.form.paymentStep == "savedCard" &&
                                                    <SavedPayment amount={props.amount} onSavedPayment={onSavedPayment} Adderss={Address.Addressform} invoicesRequest={props.invoicesRequest} />
                                                }
                                                {state.form.paymentStep == "card" &&
                                                    <>
                                                        {state.form.paymentLoading == false ? (
                                                            <Form role="form">
                                                                <FormGroup> <Label for="expMonth">
                                                                    Name
                                                                </Label>
                                                                    <InputGroup className="mb-3 w-100">

                                                                        <Input placeholder="Name" id="Name" type="text" />
                                                                    </InputGroup>
                                                                </FormGroup>
                                                                <FormGroup>
                                                                    <Label for="cardNumber">
                                                                        Card Number
                                                                    </Label>
                                                                    <div type="text" className="form-control" id="cardNumber" placeholder="XXXX-XXXX-XXXX-XXXX"></div>
                                                                </FormGroup>
                                                                <Row>
                                                                    <Col xl="6">
                                                                        <FormGroup>
                                                                            <InputGroup className="mb-3 w-100">
                                                                                <Label for="expMonth">
                                                                                    Exp. Month
                                                                                </Label>
                                                                                <UncontrolledDropdown id="expMonth" className="w-100">
                                                                                    <DropdownToggle
                                                                                        caret
                                                                                        color="secondary"
                                                                                        id="dropdownMenuButton"
                                                                                        type="button"
                                                                                        className="w-100"
                                                                                    >
                                                                                        {state.form.expMonth}
                                                                                    </DropdownToggle>
                                                                                    <DropdownMenu aria-labelledby="dropdownMenuButton">
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            01
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            02
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            03
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            04
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            05
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            06
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            07
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            08
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            09
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            10
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            11
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expMonth" onClick={(e) => toggleOption(e)} >
                                                                                            12
                                                                                        </DropdownItem>
                                                                                    </DropdownMenu>
                                                                                </UncontrolledDropdown>
                                                                            </InputGroup>
                                                                        </FormGroup>
                                                                    </Col>
                                                                    <Col xl="6">
                                                                        <FormGroup>
                                                                            <InputGroup className="mb-3 w-100">
                                                                                <Label for="expYear">
                                                                                    Exp. Year
                                                                                </Label>
                                                                                {/* <Input value="GOODYEAR TIRE CENTER" type="text" /> */}
                                                                                <UncontrolledDropdown id="expYear" className="w-100">
                                                                                    <DropdownToggle
                                                                                        caret
                                                                                        color="secondary"
                                                                                        id="dropdownMenuButton"
                                                                                        type="button"
                                                                                        className="w-100"
                                                                                    >
                                                                                        {state.form.expYear}
                                                                                    </DropdownToggle>
                                                                                    <DropdownMenu aria-labelledby="dropdownMenuButton">
                                                                                        <DropdownItem  name="expYear" onClick={(e) => toggleOption(e)} >
                                                                                            2022
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expYear" onClick={(e) => toggleOption(e)} >
                                                                                            2023
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expYear" onClick={(e) => toggleOption(e)} >
                                                                                            2024
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expYear" onClick={(e) => toggleOption(e)} >
                                                                                            2025
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expYear" onClick={(e) => toggleOption(e)} >
                                                                                            2026
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expYear" onClick={(e) => toggleOption(e)} >
                                                                                            2027
                                                                                        </DropdownItem>
                                                                                        <DropdownItem  name="expYear" onClick={(e) => toggleOption(e)} >
                                                                                            2028
                                                                                        </DropdownItem>
                                                                                    </DropdownMenu>
                                                                                </UncontrolledDropdown>
                                                                            </InputGroup>
                                                                        </FormGroup>
                                                                    </Col>
                                                                </Row>
                                                                <FormGroup>
                                                                    <Label for="securityCode-container">
                                                                        CVV
                                                                    </Label>
                                                                    <div id="securityCode-container" className="form-control" placeholder="xxx" ></div>
                                                                    <input type="hidden" id="flexresponse" value={state.form.capture} name="flexresponse" />
                                                                </FormGroup>
                                                                <div id="errors-output" role="alert" className="text-danger"></div>
                                                                <div className="text-center">
                                                                    <Button id="pay-button" className="my-4 btn-x-large" color="primary" type="button">
                                                                        Make a Payment
                                                                    </Button>
                                                                </div>
                                                            </Form>
                                                        ) : (
                                                            <div className="text-center">
                                                                <div id="loader" className="loader ">
                                                                    <span></span>
                                                                    <span></span>
                                                                    <span></span>
                                                                    <span></span>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        )}
                                                    </>
                                                }
                                                {state.form.paymentStep == "ach" &&
                                                    <Form role="form">
                                                        <FormGroup> <Label for="expMonth">
                                                            Bank Account number
                                                        </Label>
                                                            <InputGroup className="mb-3 w-100">

                                                                <Input placeholder="Account Number" id="accNo" type="text" name="accNo" onChange={(e) => achUpdate(e)} />
                                                            </InputGroup>
                                                        </FormGroup>
                                                        <FormGroup>
                                                            <Label for="routingNumber">
                                                                Bank Routing number
                                                            </Label>
                                                            <InputGroup className="mb-3 w-100">

                                                                <Input placeholder="Routing Number" name="routingNumber" id="routingNumber" type="text" onChange={(e) => achUpdate(e)} />
                                                            </InputGroup>
                                                        </FormGroup>
                                                        <FormGroup>
                                                            <InputGroup className="mb-3 w-100">
                                                                <Label for="expMonth">
                                                                    Account Type
                                                                </Label>
                                                                <UncontrolledDropdown id="accType" className="w-100">
                                                                    <DropdownToggle
                                                                        caret
                                                                        color="secondary"
                                                                        id="dropdownMenuButton"
                                                                        type="button"
                                                                        className="w-50"
                                                                    >
                                                                        {state.form.accTypeName}
                                                                    </DropdownToggle>
                                                                    <DropdownMenu aria-labelledby="dropdownMenuButton">
                                                                        <DropdownItem  value="C" name="accType" onClick={(e) => toggleOption(e)} >
                                                                            Checking
                                                                        </DropdownItem>
                                                                        <DropdownItem  value="S" name="accType" onClick={(e) => toggleOption(e)} >
                                                                            Saving
                                                                        </DropdownItem>

                                                                    </DropdownMenu>
                                                                </UncontrolledDropdown>
                                                            </InputGroup>
                                                        </FormGroup>
                                                        <Row><Col xl="6">
                                                            <FormGroup>
                                                                <input type="checkbox" className="checkbox" onChange={isChecked}></input>
                                                                <Label for="achpayment" className="ml-3">
                                                                    Save Bank details
                                                                </Label>

                                                            </FormGroup>
                                                        </Col>
                                                        </Row>
                                                        {state.form.isError &&
                                                            <div id="errors-output" role="alert" className="text-danger">One or more fields have a validation error.</div>
                                                        }
                                                        <div className="text-center">
                                                            <Button id="pay-button" onClick={() => BankPayNow()} className="my-4 btn-x-large" color="primary" type="button">
                                                                Make a Payment
                                                            </Button>
                                                        </div>
                                                    </Form>
                                                }
                                                {state.form.paymentStep == "confirm" && (
                                                    <Col xl="12">
                                                        <Row className="align-items-center">
                                                            <div className="col-12 mx-auto">
                                                                <h3 className="text-center">Confirm Payment</h3>
                                                                <h4 className="text-center">Do you want to proceed to payment ?</h4>
                                                                {state.form.AddressError &&
                                                                    <div id="errors-output" className="text-danger text-center" role="alert">{state.form.responseMessage}</div>
                                                                }
                                                                <div className="text-center">
                                                                    <Button onClick={() => PayNow()} id="confirm-button" className="my-4 btn-x-large" color="primary" type="button">
                                                                        Confirm
                                                                    </Button>
                                                                </div>
                                                            </div>
                                                        </Row>
                                                    </Col>
                                                )}
                                                {state.form.paymentStep == "success" && (
                                                    <Col xl="12">
                                                        <Card className="shadow">
                                                            <Row className="align-items-center">
                                                                <div className="col-6 mx-auto">
                                                                    <h3 className="text-center">{state.form.responseMessage}</h3>
                                                                    <center>
                                                                        {state.form.success == 1 &&
                                                                            <div className="mt-3 mb-3" style={{ boxShadow: '5px 5px 5px 0 rgb(0 0 0 / 13%), -5px -5px 5px 0 rgb(255 255 255 / 29%)', borderRadius: '50%', height: '50px', width: '50px', lineHeight: '50px', fontSize: '30px' }}>
                                                                                <i className="ni ni-check-bold"></i>

                                                                            </div>
                                                                        }
                                                                        {state.form.success == 2 || state.form.success == 3 &&
                                                                            <div className="mt-3 mb-3" style={{ boxShadow: '5px 5px 5px 0 rgb(0 0 0 / 13%), -5px -5px 5px 0 rgb(255 255 255 / 29%)', borderRadius: '50%', height: '50px', width: '50px', lineHeight: '50px', fontSize: '30px' }}>
                                                                                <i className="ni ni-fat-remove" style={{ color: '#ea5353' }}></i>
                                                                            </div>
                                                                        }
                                                                    </center>
                                                                    {state.form.success != 3 ? (
                                                                        <pre>{state.form.transactionID}</pre>
                                                                    ) : (
                                                                        <></>
                                                                    )}
                                                                </div>
                                                            </Row>
                                                        </Card>
                                                    </Col>
                                                )}
                                            </Col>
                                        </Row>
                                    </CardBody>
                                </Card>
                            </Col>
                            {/* Payment div start */}
                        </Row>
                    }
                    {state.form.mainTab == "success" &&
                        <Row >
                            <Col xl="12">
                                <Card className="shadow" style={{ height: "70vh" }}>
                                    <Row className="align-items-center mt-5">
                                        <div className="col-6 mx-auto mt-5">
                                            <h3 className="text-center">{state.form.responseMessage}</h3>
                                            <center>
                                                {state.form.success == 1 &&
                                                    <div className="mt-3 mb-3" style={{ boxShadow: '5px 5px 5px 0 rgb(0 0 0 / 13%), -5px -5px 5px 0 rgb(255 255 255 / 29%)', borderRadius: '50%', height: '50px', width: '50px', lineHeight: '50px', fontSize: '30px' }}>
                                                        <i className="ni ni-check-bold"></i>

                                                    </div>
                                                }
                                                {state.form.success == 2 || state.form.success == 3 &&
                                                    <div className="mt-3 mb-3" style={{ boxShadow: '5px 5px 5px 0 rgb(0 0 0 / 13%), -5px -5px 5px 0 rgb(255 255 255 / 29%)', borderRadius: '50%', height: '50px', width: '50px', lineHeight: '50px', fontSize: '30px' }}>
                                                        <i className="ni ni-fat-remove" style={{ color: '#ea5353' }}></i>
                                                    </div>
                                                }
                                            </center>
                                            {state.form.success != 3 ? (
                                                <pre className="text-center">Transaction Id :{state.form.transactionID != "" ? state.form.transactionID : state.form.reconciliationId}</pre>
                                            ) : (
                                                <></>
                                            )}
                                        </div>
                                    </Row>
                                </Card>
                            </Col>
                        </Row>
                    }
                </Container>
            </ModalBody>
            <ModalFooter>
            </ModalFooter>
        </Modal>
    )
}
export default PaymentModal;