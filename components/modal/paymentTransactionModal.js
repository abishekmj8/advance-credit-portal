import React, { useEffect } from "react";
import { Grid, h } from 'gridjs-react';
import "gridjs/dist/theme/mermaid.css";
// reactstrap components
import {
    Row,
    Col,
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    Container
} from "reactstrap";
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
const PaymentTransactionModal = (props) => {
    return (
        <Modal toggle={() => props.toggle()} isOpen={props.modalOpen} size="lg" className="modal-size modal-xl modal-xl-full-screen modal-lg">
            <div className=" modal-header w-90">
                <h3 className=" modal-title" id="exampleModalLabel">
                    Payment Details
                </h3>
                <button
                    aria-label="Close"
                    className=" close"
                    type="button"
                    onClick={() => props.toggle()}
                >
                    <span aria-hidden={true}>×</span>
                </button>
            </div>
            <ModalBody >
                {props.modalData ?
                    <Container fluid>
                        <h2>Payment Header</h2>
                        <Row>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Customer Account Number</h5>
                                    <span className="modal-label-span-text">{props.modalData.customerAccountNumber}</span>
                                </div>


                                <div className="d-flex justify-content-between">
                                    <h5>Payment Amount</h5>
                                    <span className="modal-label-span-text">${props.modalData.paymentAmount}</span>
                                </div>
                            </Col>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Payment #</h5>
                                    <span className="modal-label-span-text">{props.modalData.paymentNumber}</span>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5> Unapplied Amount</h5>
                                    <span className="modal-label-span-text">${props.modalData.unappliedAmount}</span>
                                </div>
                            </Col>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Payment Method</h5>
                                    <span className="modal-label-span-text">{props.modalData.paymentMethod}</span>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5>Status</h5>
                                    <span className="modal-label-span-text">{props.modalData.status}</span>
                                </div>
                            </Col>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Payment date</h5>
                                    <span className="modal-label-span-text">{props.modalData.paymentDate}</span>
                                </div>
                            </Col>
                        </Row>
                        <h2>Payment Details</h2>
                        <Row className="justify-content-center">
                            <Col className="mb-5 mb-xl-0" xl="12">
                                <Grid
                                    data={props.tansactionDetails.transactionPaymentDetails}
                                    columns={[
                                        {
                                            data: (data) => data.transactionId,
                                            name: "Transaction Id",
                                        },
                                        {
                                            data: (data) => data.appliedTo,
                                            name: "Applied To",
                                        },
                                        {
                                            data: (data) => data.appliedToTransaction,
                                            name: "Applied To Transaction",
                                        },
                                        {
                                            data: (data) => data.appliedAmount,
                                            name: "Applied Amount",
                                        },
                                        {
                                            data: (data) => data.appliedDate,
                                            name: "Applied Date",
                                        },
                                    ]}
                                    search={false}
                                    sort={false}
                                    resizable={true}
                                    pagination={{
                                        enabled: false,
                                        limit: 7,
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xl="8"></Col>
                            <Col xl="4" className=" justify-content-end mt-2">

                                <div className="d-flex justify-content-between">
                                    <h5>Total Applied Amount</h5>
                                    <span className="modal-label-span-text">$ {props.tansactionDetails.totalAppliedAmount}</span>
                                </div>
                            </Col>
                        </Row>
                        <h2>Transaction Details</h2>
                        <Row className="justify-content-center">
                            <Col className="mb-5 mb-xl-0" xl="12">
                                <p className="">Payments not applied to any transactions yet</p>
                                {/* <Grid
                                    data={[
                                        ['', '', '', '', '', ''],
                                    ]}
                                    columns={['Payment Date', 'Applicaction Type', 'Transaction Number', 'Applied Date', 'Quantity', 'Amount Applied']}
                                    search={false}
                                    sort={false}
                                    resizable={true}
                                    pagination={{
                                        enabled: false,
                                        limit: 7,
                                    }}
                                /> */}
                            </Col>
                        </Row>
                    </Container>
                    :
                    <Skeleton height={30} />
                }
            </ModalBody>
            <ModalFooter>
                <Button
                    color="secondary"
                    type="button"
                    onClick={() => props.toggle()}
                >
                    Cancel
                </Button>
            </ModalFooter>
        </Modal>
    )
}
export default PaymentTransactionModal;