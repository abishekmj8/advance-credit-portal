import React, { useEffect } from "react";
import { Grid, h } from 'gridjs-react';
import "gridjs/dist/theme/mermaid.css";
// reactstrap components
import {
    Row,
    Col,
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    Container
} from "reactstrap";
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
const creditMemoModal = (props) => {
    return (
        <Modal toggle={() => props.toggle()} isOpen={props.modalOpen} size="lg" className="modal-size modal-xl modal-xl-full-screen modal-lg">
            <div className=" modal-header w-90">
                <h3 className=" modal-title" id="exampleModalLabel">
                    Credit Memo Details
                </h3>
                <button
                    aria-label="Close"
                    className=" close"
                    type="button"
                    onClick={() => props.toggle()}
                >
                    <span aria-hidden={true}>×</span>
                </button>
            </div>
            <ModalBody >
                {props.modalData ?
                    <Container fluid>
                        <h2>Transaction Header</h2>
                        <Row>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Customer Account Number</h5>
                                    <span className="modal-label-span-text">{props.modalData.customerAccountNumber}</span>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5> Transaction Amount</h5>
                                    <span className="modal-label-span-text">$ {props.modalData.originalAmount}</span>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5>Payment Term</h5>
                                    <span className="modal-label-span-text">{props.modalData.paymentTerms}</span>
                                </div>
                            </Col>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Transaction #</h5>
                                    <span className="modal-label-span-text">{props.modalData.transactionId}</span>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5> Due Date</h5>
                                    <span className="modal-label-span-text">{props.modalData.dueDate}</span>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5>Transaction Status</h5>
                                    <span className="modal-label-span-text">{props.modalData.transactionStatus}</span>
                                </div>
                            </Col>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Type</h5>
                                    <span className="modal-label-span-text">{props.modalData.transactionType}</span>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5> Transaction Balance</h5>
                                    <span className="modal-label-span-text">$ {props.modalData.balanceAmount}</span>
                                </div>
                            </Col>
                            <Col xl="3">
                                <div className="d-flex justify-content-between">
                                    <h5> Transaction date</h5>
                                    <span className="modal-label-span-text">{props.modalData.transactionDate}</span>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5> PO #</h5>
                                    <span className="modal-label-span-text">{props.modalData.purchaseOrder}</span>
                                </div>
                            </Col>
                        </Row>
                        <h2>Transaction Details</h2>
                        <Row className="justify-content-center">
                            <Col className="mb-5 mb-xl-0" xl="12">
                                <Grid
                                    data={props.tansactionDetails.transactionDetails}
                                    columns={[
                                        {
                                            data: (data) => data.lineNumber,
                                            name: "Line Number",
                                        },
                                        {
                                            data: (data) => data.productLine,
                                            name: "Product Line",
                                        },
                                        {
                                            data: (data) => data.lineDescription,
                                            name: "Description",
                                        },
                                        {
                                            data: (data) => data.sku,
                                            name: "SKU",
                                        },
                                        {
                                            data: (data) => data.uom,
                                            name: "UOM",
                                        },
                                        {
                                            data: (data) => data.quantity,
                                            name: "Quantity",
                                        },

                                        {
                                            data: (data) => data.unitPrice,
                                            name: "Unit Price",
                                        },
                                        {
                                            data: (data) => data.lineAmount,
                                            name: "Line Amount",
                                        },
                                    ]}
                                    search={false}
                                    sort={false}
                                    resizable={true}
                                    pagination={{
                                        enabled: false,
                                        limit: 7,
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xl="8"></Col>
                            <Col xl="4" className=" justify-content-end mt-2">
                                <div className="d-flex justify-content-between">
                                    <h5> Sub Total</h5>
                                    <span className="modal-label-span-text"> $ {props.tansactionDetails.subTotalAmount}</span>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5>Total Sales / Use Tax</h5>
                                    <span className="modal-label-span-text">$ {props.tansactionDetails.useTaxTotalAmount}</span>
                                </div>

                                <div className="d-flex justify-content-between">
                                    <h5>Total Transaction Amount</h5>
                                    <span className="modal-label-span-text">$ {props.tansactionDetails.transactionTotalAmount}</span>
                                </div>
                            </Col>
                        </Row>
                        <h2>Transaction Activities</h2>
                        <Row className="justify-content-center">
                            <Col className="mb-5 mb-xl-0" xl="12">
                                <p className="">No Payments Applied Yet</p>
                                {/* <Grid
                                    data={[
                                        ['', '', '', '', '', ''],
                                    ]}
                                    columns={['Payment Date', 'Applicaction Type', 'Transaction Number', 'Applied Date', 'Quantity', 'Amount Applied']}
                                    search={false}
                                    sort={false}
                                    resizable={true}
                                    pagination={{
                                        enabled: false,
                                        limit: 7,
                                    }}
                                /> */}
                            </Col>
                        </Row>
                    </Container>
                    :
                    <Skeleton height={30} />
                }
            </ModalBody>
            <ModalFooter>
                <Button
                    color="secondary"
                    type="button"
                    onClick={() => props.toggle()}
                >
                    Cancel
                </Button>
            </ModalFooter>
        </Modal>
    )
}
export default creditMemoModal;