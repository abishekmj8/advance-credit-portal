import { Grid, _ } from "gridjs-react";
import "gridjs/dist/theme/mermaid.css";
import Link from "next/link";
const TransactionGrid = ({ data, switchModal }) => {


  return (
    <div className="grid-size justify-content-center" >


      <Grid id='data-grid'
        data={data}
        columns={[{
          data: (data) => data.transactionDate,
          name: "Date",
          attributes: {
            'title':  'Date'
          }
        },
        {
          data: (data) => data.transactionType,
          name: "Type",
          attributes: {
            'title':  'Type'
          }
        },
        {
          data: (data) => data.purchaseOrder,
          name: "PO #",
          attributes: {
            'title':  'PO #'
          }
        },
        {
          data: (data) => _(<a href="#" onClick={() => switchModal(data)}>{data.transactionNumber}</a>),
          name: "Transaction #",
          attributes: {
            'title':  'Transaction #'
          }
        },
        {
          data: (data) => data.originalAmount,
          name: "Amount",
          attributes: {
            'title':  'Amount'
          }
        },
        {
          data: (data) => data.transactionStatus,
          name: "Status",
          attributes: {
            'title':  'Status'
          }
        },
        ]}
        search={false}

        sort={true}
        resizable={true}
        pagination={{
          enabled: true,
          limit: 11,
        }}
      />
    </div>
  )
}
export default TransactionGrid
