const path = require('path')

module.exports = {
  images: {
    disableStaticImages: true
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles/scss')],
  },
  experimental: {
    outputStandalone: true,
  },
  webpack(config, options) {
      config.module.rules.push({
          test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
          use: {
              loader:require.resolve("file-loader") + "?name=../[path][name].[ext]",
              options: {
                  limit: 100000
              }
          }
      });

      return config;
  }
};